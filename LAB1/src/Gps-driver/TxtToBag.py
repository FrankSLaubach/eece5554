#!/usr/bin/env python

import rospy
from std_msgs.msg import String

from lab1_f.msg import gpsMsgFormat
import utm

#I am making this function to translate the .txt files I collected with my GPS driver to rosbags. I did not know how to use rosbag at the time. This program was made following this tutorial: https://www.pythontutorial.net/python-basics/python-read-text-file/, tutorials 10 & 11 in: http://wiki.ros.org/ROS/Tutorials, and by looking at this rosbag tutorial: http://wiki.ros.org/rosbag/Tutorials/Recording%20and%20playing%20back%20data
def TxtToBag():
    pub = rospy.Publisher('main', gpsMsgFormat, queue_size=10)
    rospy.init_node('translate', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    
    #I did this for 2 files, 'still' and 'moving'
    f = open('/home/frank_laubach/catkin_ws/src/lab1_f/src/data/moving.txt','r')
    contents = f.readlines()
    f.close()
    
    rospy.sleep(10) #to give me enough time to activate a rosbag
    
    for x in contents:
    	split = x.split(' ')
    	time = float(split[1][1:len(split[1])-2])
    	latitude = float(split[5])
    	longitude = -1*float(split[7])
    	
    	AllGPSData = gpsMsgFormat()
    	AllGPSData.Header = "GPGGA & UTM Formats: "
    	AllGPSData.timeRec = time
    	AllGPSData.Latitude = latitude
    	AllGPSData.Longitude = longitude
    	AllGPSData.Altitude = float(split[9])
    	
    	try:
    		utm_data = utm.from_latlon(round(latitude/100,8), round(longitude/100,8))
    	except utm.error.OutOfRangeError: 
    		utm_data = [0,0,0,0]
    	
    	AllGPSData.UTM_Easting = utm_data[0]
    	AllGPSData.UTM_Northing = utm_data[1]
    	AllGPSData.UTM_Zone_Num = str(utm_data[2])
    	AllGPSData.UTM_Zone_Let = str(utm_data[3])
    	
    	rospy.loginfo(AllGPSData)
    	pub.publish(AllGPSData)
    
    #I caught all the output in a rosbag

if __name__ == '__main__':
    try:
        TxtToBag()
    except rospy.ROSInterruptException:
        pass


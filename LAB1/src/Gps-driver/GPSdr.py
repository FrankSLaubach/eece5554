#!/usr/bin/env python

import rospy
from std_msgs.msg import String

import serial
from math import sin, pi

import utm

#This file was created in a subfolder of the catkin_tutorials folder, following tutorial 3 in http://wiki.ros.org/ROS/Tutorials

#The def section is based on the talker from tutorial 12 in http://wiki.ros.org/ROS/Tutorials. It receives raw data from the main section below, and extracts Lat, Long, and Alt from it. It then calculates the utm data, and publishes all the 7 variables and their header in one string.
#the publisher works, but I didn't know how to use rosbag at the time (I had the publisher set up correctly, but I would try recording the rosbag BEFORE I ran the publisher) so I saved the terminal output in a .txt file. I don't have time to capture more data, I was the first in the team to write the driver. I am going to write another publisher to go through that text file and publish the 7 variables, and record them in a rosbag. I am also going to recalculate the UTM variables while doing so because I didn't make longitude negative in this code, so the GPS data says I am in Kazakhastan. Very Nice.
def GPSdr(raw_data):
    pub = rospy.Publisher('AllGPSData', String, queue_size=10)
    rospy.init_node('GPSdr', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    
    #isolating GPGGA from the raw data, which is a giant chunk of text
    iter0=str(raw_data)
    num1 = iter0.find('$GPGGA')+7      
    num2 = iter0.find('$GPGSA',num1)-4 #to chop the/r/n off the end
    iter1 = iter0[num1:num2]
    iter2 = iter1.split(',')
    
    #extract lat long alt
    #sometimes the gps outputs no data, so this section uses 0 as placeholder values in case no data can be found.
    latitude = 0.0
    longitude = 0.0
    altitude = 0.0
    if len(iter2)>9:
	    if check_float(iter2[1]) and len(iter2)>9:
	    	latitude = float(iter2[1])
	    if check_float(iter2[3]) and len(iter2)>9:
	    	longitude = float(iter2[3])
	    if check_float(iter2[8]) and len(iter2)>9:
	    	altitude = iter2[8]
    
    #calculate utm_data, with an error catcher.
    try:
    	utm_data = utm.from_latlon(round(latitude/100,1), round(longitude/100,1))
    except utm.error.OutOfRangeError: 
    	utm_data = [0,0,0,0]
    utm_easting = utm_data[0]
    utm_northing = utm_data[1]
    zoneNum = utm_data[2]
    zoneLet = utm_data[3]
    
    #print / publish
    hello_str = "GPS Data: Lat: " + str(latitude) + " Long: " + str(longitude) +" Alt: " + str(altitude) + " UTM East: " + str(utm_easting) +" UTM North: " + str(utm_northing) + " Zone: " + str(zoneNum) + str(zoneLet)
    rospy.loginfo(hello_str)
    pub.publish(hello_str)
    rate.sleep()
	
#https://www.kite.com/python/answers/how-to-check-if-a-string-is-a-valid-float-in-python
def check_float(potential_float):
    try:
        float(potential_float)
        return True
    except ValueError:
        return False

#this section is from the code in Lab_1_spring_2022. it just reads the GPS code.
if __name__ == '__main__':

    serial_port = rospy.get_param('~port','/dev/ttyUSB0')
    serial_baud = rospy.get_param('~baudrate',4800)
    
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
    
    rospy.sleep(0.2)        
    line = port.readline()

    try:
        while not rospy.is_shutdown():
            line = port.read(300) #readline was only catching the first line in the wrong format. instead i am going to read a giant chunk of text, and extract out the GPGGA section in the GPSdr function definition above.
            GPSdr(line)
            rospy.sleep(.2)
            
        rospy.sleep(0.2)  
        #rospy.sleep(sleep_time)

    except rospy.ROSInterruptException:
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down node...")


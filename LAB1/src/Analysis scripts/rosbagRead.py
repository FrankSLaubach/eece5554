#source for reading rosbags: http://wiki.ros.org/rosbag/Code%20API
#source for initializing lists: https://www.programiz.com/python-programming/list
#source for appending lists: https://www.w3schools.com/python/python_lists_add.asp
#source for plots: https://www.geeksforgeeks.org/graph-plotting-in-python-set-1/
#source for rospy time: http://wiki.ros.org/rospy/Overview/Time
#source for best fit line: https://www.kite.com/python/answers/how-to-plot-a-line-of-best-fit-in-python
#source for std dev: https://www.pythonpool.com/numpy-standard-deviation/
#source for R^2 value: https://stackoverflow.com/questions/22239691/code-for-best-fit-straight-line-of-a-scatter-plot-in-python

#i am running this code with 'python3 rosbagRead.py', not through rosrun

import rosbag
import matplotlib.pyplot as plt
import numpy as np

#opening the bags
moving = rosbag.Bag('/home/frank_laubach/catkin_ws/src/lab1_f/src/data/moving.bag')
still = rosbag.Bag('/home/frank_laubach/catkin_ws/src/lab1_f/src/data/still.bag')

#initializing lists
timeListM=[]
latListM=[]
longListM=[]
altListM=[]
utmEListM=[]
utmNListM=[]
utmZNListM=[]
utmZLListM=[]

timeListS=[]
latListS=[]
longListS=[]
altListS=[]
utmEListS=[]
utmNListS=[]
utmZNListS=[]
utmZLListS=[]

#reading the bags
timeTrigger = 0
for topic, msg, t in moving.read_messages(topics=['main']):
    if msg.UTM_Zone_Let == "T": #due to how I wrote my code, blank readings were recorded as 0's in the rosbag. I am removing them here.
    	if timeTrigger == 0:
    	    timeTrigger = 1
    	    timeStart = msg.timeRec
    	timeListM.append(msg.timeRec-timeStart)
    	latListM.append(msg.Latitude)
    	longListM.append(msg.Longitude)
    	altListM.append(msg.Altitude)
    	utmEListM.append(msg.UTM_Easting)
    	utmNListM.append(msg.UTM_Northing)
    	utmZNListM.append(msg.UTM_Zone_Num)
    	utmZLListM.append(msg.UTM_Zone_Let)
moving.close()

timeTrigger = 0
for topic, msg, t in still.read_messages(topics=['main']):
    if msg.UTM_Zone_Let == "T": #due to how I wrote my code, blank readings were recorded as 0's in the rosbag. I am removing them here.
    	if timeTrigger == 0:
    	    timeTrigger = 1
    	    timeStart = msg.timeRec
    	timeListS.append(msg.timeRec-timeStart)
    	latListS.append(msg.Latitude)
    	longListS.append(msg.Longitude)
    	altListS.append(msg.Altitude)
    	utmEListS.append(msg.UTM_Easting)
    	utmNListS.append(msg.UTM_Northing)
    	utmZNListS.append(msg.UTM_Zone_Num)
    	utmZLListS.append(msg.UTM_Zone_Let)
moving.close()

#plotting 
plt.scatter(latListM, longListM,s=1)
plt.xlabel('Latitude')
plt.ylabel('Longitude')
plt.title('Latitude vs Longitude while Moving')
plt.show()

plt.scatter(latListS, longListS,s=1)	
plt.xlabel('Latitude')
plt.ylabel('Longitude')
plt.title('Latitude vs Longitude while Still')
plt.show()

plt.scatter(timeListM, altListM,s=1)
plt.xlabel('Time (s)')
plt.ylabel('Altitude (m)')
plt.title('Altitude vs Time while Moving')
plt.show()

plt.scatter(timeListS, altListS,s=1)
plt.xlabel('Time (s)')
plt.ylabel('Altitude (m)')
plt.title('Altitude vs Time while Still')
plt.show()

plt.scatter(utmEListM, utmNListM,s=1)
plt.xlabel('UTM Easting')
plt.ylabel('UTM Northing')
plt.title('UTM Easting vs Northing while Moving')
plt.show()

plt.scatter(utmEListS, utmNListS,s=1)
plt.xlabel('UTM Easting')
plt.ylabel('UTM Northing')
plt.title('UTM Easting vs Northing while Still')
plt.show()

#Moving UTM with best fit line
plt.scatter(utmEListM, utmNListM,s=1)
plt.scatter(utmEListM[105:210], utmNListM[105:210],s=1)

x = np.array(utmEListM[105:210])
y = np.array(utmNListM[105:210])
m, b = np.polyfit(x,y,1)
plt.plot(x,m*x+b)
variance = np.var(y)
residuals = np.var(m*x+b-y)
Rsqr = np.round(1-residuals/variance, decimals=5)
plt.text(.9*max(x)+.1*min(x),.9*max(y)+.1*min(y),'$R^2 = %0.5f$'% Rsqr, fontsize=10)
print("UTM Moving R^2: " + str(Rsqr))
print("UTM Moving Residual Standard Deviation: "+str(residuals))

error = [((m*x+b)[h]-y[h]) for h in range (len(m*x+b))]
#print(error)
print("UTM Moving Mean Error: "+str(np.mean(error)))
print("UTM Moving Northing Mean:"+str(np.mean(y)))
print("UTM Mean Error / Northing: "+str(np.mean(error)/np.mean(y)))

plt.xlabel('UTM Easting')
plt.ylabel('UTM Northing')
plt.title('UTM Easting vs Northing while Moving')
plt.show()

#Still UTM with Standard Deviation
plt.scatter(utmEListS, utmNListS,s=1)
x = np.array(utmEListS)
y = np.array(utmNListS)
a = np.array([x,y])
stdevA = np.std(a)
meanA = np.mean(a)
print("Overall Standard Deviation: "+ str(stdevA))
print("Overall Mean: " + str(meanA))
print("Overall STDev / Mean ratio: " + str(stdevA/meanA))
stdevX = np.std(x)
meanX = np.mean(x)
print("Easting Standard Deviation: "+ str(stdevX))
print("Easting Mean: " + str(meanX))
print("Easting STDev / Mean ratio: " + str(stdevX/meanX))
stdevY = np.std(y)
meanY = np.mean(y)
print("Northing Standard Deviation: "+ str(stdevY))
print("Northing Mean: " + str(meanY))
print("Northing STDev / Mean ratio: " + str(stdevA/meanY))
plt.xlabel('UTM Easting')
plt.ylabel('UTM Northing')
plt.title('UTM Easting vs Northing while Moving')
plt.show()

%EECE 5554 Lab 1
%https://www.mathworks.com/help/ros/ref/rosbag.html

%DEFINING BAGS, TOPICS, STRUCTURE
still = rosbag('still.bag');
moving = rosbag('moving.bag');
%bagInfo = rosbag('info','still.bag')
%rosbag info 'still.bag'
stillTopic = select(still,'Topic','main');
stillRead = readMessages(stillTopic,'DataFormat','struct');
stillRead{1}
stillRead{2}
stillRead{3}
movingTopic = select(moving,'Topic','main');
movingRead = readMessages(movingTopic,'DataFormat','struct');

%EXCTRACTING STILL BAG DATA
latS = cellfun(@(m) double(m.Latitude),stillRead);
longS = cellfun(@(m) double(m.Longitude),stillRead);
altS = cellfun(@(m) double(m.Altitude),stillRead);
utmES = cellfun(@(m) double(m.UTMEasting),stillRead);
utmNS = cellfun(@(m) double(m.UTMNorthing),stillRead);
utmZNS = cellfun(@(m) string(m.UTMZoneNum),stillRead);
utmZLS = cellfun(@(m) string(m.UTMZoneLet),stillRead);
timeS = cellfun(@(m) double(m.TimeRec),stillRead); 

%i am going to remove 0 values
latS2=[]
longS2=[]
altS2=[]
utmES2=[]
utmNS2=[]
utmZNS2=[]
utmZLS2=[]
timeS2=[]
for a = 1:size(utmZLS,1)
    if utmZLS(a) == "T"
        latS2=[latS2;latS(a)];
        longS2=[longS2;longS(a)];
        altS2=[altS2;altS(a)];
        utmES2=[utmES2;utmES(a)];
        utmNS2=[utmNS2;utmNS(a)];
        utmZNS2=[utmZNS2;utmZNS(a)];
        utmZLS2=[utmZLS2;utmZLS(a)];
        timeS2=[timeS2;timeS(a)];
    end
end

%EXTRACTING LONG BAG DATA
latM = cellfun(@(m) double(m.Latitude),movingRead);
longM = cellfun(@(m) double(m.Longitude),movingRead);
altM = cellfun(@(m) double(m.Altitude),movingRead);
utmEM = cellfun(@(m) double(m.UTMEasting),movingRead);
utmNM = cellfun(@(m) double(m.UTMNorthing),movingRead);
utmZNM = cellfun(@(m) string(m.UTMZoneNum),movingRead);
utmZLM = cellfun(@(m) string(m.UTMZoneLet),movingRead);
timeM = cellfun(@(m) double(m.TimeRec),movingRead); 

%i am going to remove 0 values
latM2=[]
longM2=[]
altM2=[]
utmEM2=[]
utmNM2=[]
utmZNM2=[]
utmZLM2=[]
timeM2=[]
for a = 1:size(utmZLM,1)
    if utmZLM(a) == "T"
        latM2=[latM2;latM(a)];
        longM2=[longM2;longM(a)];
        altM2=[altM2;altM(a)];
        utmEM2=[utmEM2;utmEM(a)];
        utmNM2=[utmNM2;utmNM(a)];
        utmZNM2=[utmZNM2;utmZNM(a)];
        utmZLM2=[utmZLM2;utmZLM(a)];
        timeM2=[timeM2;timeM(a)];
    end
end

%STIL VALUES
disp("latS: "+mean(latS2)+", "+std(latS2)+", "+std(latS2)/mean(latS2))
disp("longS: "+mean(longS2)+", "+std(longS2)+", "+std(longS2)/mean(longS2))
disp("altS: "+mean(altS2)+", "+std(altS2)+", "+std(altS2)/mean(altS2))
disp("utmES: "+mean(utmES2)+", "+std(utmES2)+", "+std(utmES2)/mean(utmES2))
disp("utmNS: "+mean(utmNS2)+", "+std(utmNS2)+", "+std(utmNS2)/mean(utmNS2))

%MOVING VALUES
disp("latM: "+mean(latM2)+", "+std(latM2)+", "+std(latM2)/mean(latM2))
disp("longM: "+mean(longM2)+", "+std(longM2)+", "+std(longM2)/mean(longM2))
disp("altM: "+mean(altM2)+", "+std(altM2)+", "+std(altM2)/mean(altM2))
disp("utmEM: "+mean(utmEM2)+", "+std(utmEM2)+", "+std(utmEM2)/mean(utmEM2))
disp("utmNM: "+mean(utmNM2)+", "+std(utmNM2)+", "+std(utmNM2)/mean(utmNM2))
%
%plot(utmES, utmNS)

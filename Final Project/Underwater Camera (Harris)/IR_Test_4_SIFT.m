%EECE 5554 Final AR SLAM Test Code 4

clear all
close all

%READ DATA
    bagPath = "uas_ir_day.bag";
    bagSelect = rosbag(bagPath);
    bagInfo = rosbag('info',bagPath);

    cameraInfoTopic = select(bagSelect,'Topic','uas4/camera_info');
    cameraInfoRead = readMessages(cameraInfoTopic,'DataFormat','struct');
    cameraInfoRead{1};

    imageRawTopic = select(bagSelect,'Topic','uas4/image_raw');
    imageRawRead = readMessages(imageRawTopic,'DataFormat','struct');
    imageRawRead{1};

 %FIX DISTORTION
    %https://www.mathworks.com/help/ros/ref/readimage.html
    distortedImg = rosReadImage(imageRawRead{1});
    %https://www.mathworks.com/help/vision/ref/undistortimage.html
    IntrinsicMatrix = [ 524.888150200348832 0                   0; ...
                        0                   521.776791343664968 0; ...
                        325.596989785447420 242.392342491041603 1];
    radialDistortion = [-0.470302508060718438   0.301057860458473880]; 
    tangentialDistortion = [0.00468835914496582538	0.00165573977268025185];
    imageSize = [512,640];
    cameraParams = cameraParameters('IntrinsicMatrix',IntrinsicMatrix,...
        'RadialDistortion',radialDistortion,...
        'TangentialDistortion',tangentialDistortion,...
        'ImageSize',imageSize); 
    [undistortedImg, origin] = undistortImage(distortedImg,cameraParams);

    figure('Name','Remove Distortion Ex');
    montage(cat(3,distortedImg,undistortedImg));

%LOAD IMAGES
    %https://www.mathworks.com/help/matlab/math/multidimensional-arrays.html
    testLength = 208; %max is 700. try 360, 156, 180, 200
    testImgs = [];
    for i = 1:10:testLength*10
       image = rosReadImage(imageRawRead{i});
       [image, origin] = undistortImage(image,cameraParams);
       testImgs = cat(3,testImgs,image);
    end
    figure('Name','Montage');
    montage(testImgs);

%LAB 5 HARRIS CORNER METHOD
    %changes made:
    %I = testImgs(:,:,1);
    %numImages = testLength;
    %tforms(numImages) = affine2d(eye(3));
    %panorama = zeros([height width 3], 'like', I); %removed 3 for grayscale
    %harris changed to harris_mod (boolean for show image)
    % added adapthisteq to improve grayscale contrast for features
    % used adapthisteq functions: numTiles [4 4] and clipLimit 0.005
    % changed harris features from 1000 to 2000, [2 2] to [4 4]
    
    %Panorama array
    %Points for the panorama array

    % Read the first image from the image set.
    I = testImgs(:,:,1);

    % Initialize features for I(1)
    I = adapthisteq(I,'clipLimit',0.01);
    grayImage = im2gray(I);
    %grayImage = adapthisteq(grayImage,'clipLimit',0.01); % Increase gray contrast
    [y,x,m] = harris_mod(grayImage,true,1000,'tile',[2 2],'disp');
    points = [x,y];
    [features, points] = extractFeatures(grayImage,points);

    % Initialize all the transforms to the identity matrix. Note that the
    % projective transform is used here because the building images are fairly
    % close to the camera. Had the scene been captured from a further distance,
    % an affine transform would suffice.
    numImages = testLength;
    tforms(numImages) = affine2d(eye(3));

    % Initialize variable to hold image sizes.
    imageSize = zeros(numImages,2);

    % Iterate over remaining image pairs
    for n = 2:numImages

        % Store points and features for I(n-1).
        pointsPrevious = points;
        featuresPrevious = features;

        % Read I(n).
        I = testImgs(:,:,n);

        % Convert image to grayscale.
        I = adapthisteq(I,'clipLimit',0.01);
        grayImage = im2gray(I);
        %grayImage = adapthisteq(grayImage,'clipLimit',0.01); % Increase gray contrast

        % Save image size.
        imageSize(n,:) = size(grayImage);

        % Detect and extract Harris.
        [y,x,m] = harris_mod(grayImage,false,1000,'tile',[2 2],'disp');
        points = [x,y];
        [features, points] = extractFeatures(grayImage,points);

        % Find correspondences between I(n) and I(n-1).
        indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);

        matchedPoints = points(indexPairs(:,1), :);
        matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);        

        % Estimate the transformation between I(n) and I(n-1).
        tforms(n) = estimateGeometricTransform2D(matchedPoints, matchedPointsPrev,...
            'affine', 'Confidence', 99.9, 'MaxNumTrials', 2000);

        % Compute T(n) * T(n-1) * ... * T(1)
        tforms(n).T = tforms(n).T * tforms(n-1).T; 
    end

    % Compute the output limits for each transform.
    for i = 1:numel(tforms)           
        [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);    
    end

    avgXLim = mean(xlim, 2);
    [~,idx] = sort(avgXLim);
    centerIdx = floor((numel(tforms)+1)/2);
    centerImageIdx = idx(centerIdx);

    Tinv = invert(tforms(centerImageIdx));
    for i = 1:numel(tforms)    
        tforms(i).T = tforms(i).T * Tinv.T;
    end

    for i = 1:numel(tforms)           
        [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);
    end

    maxImageSize = max(imageSize);

    % Find the minimum and maximum output limits. 
    xMin = min([1; xlim(:)]);
    xMax = max([maxImageSize(2); xlim(:)]);

    yMin = min([1; ylim(:)]);
    yMax = max([maxImageSize(1); ylim(:)]);

    % Width and height of panorama.
    width  = round(xMax - xMin);
    height = round(yMax - yMin);

    % Initialize the "empty" panorama.
    panorama = zeros([height width], 'like', I);

    blender = vision.AlphaBlender('Operation', 'Binary mask', ...
        'MaskSource', 'Input port');  

    % Create a 2-D spatial reference object defining the size of the panorama.
    xLimits = [xMin xMax];
    yLimits = [yMin yMax];
    panoramaView = imref2d([height width], xLimits, yLimits);

    %ADD: Panorama Array
    camW = cameraParams.ImageSize(2);
    camH = cameraParams.ImageSize(1);
    partPanoramas = [];
    coordTL = [];
    coordCent = [];
    coordTR = [];
    coordBL = [];
    coordBR = [];
    markedPartPanoramas = [];
    xPosPix = [];
    yPosPix = [];
    distPix = [];
    xVelPix = [0];
    yVelPix = [0];    
    velPix = [0];
    angle = [];    
    leftLScale = [];
    topLScale = [];
       
    % Create the panorama.
    for i = 1:numImages

        I = testImgs(:,:,i);

        % Transform I into the panorama.
        warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);      

        %Collect coordinates of corners and center
        %note that panorama center is at -xLimits(1), -yLimits(1)
        %note that MATLAB coords are measured from the top left corner
        tFormsMatrix = tforms(i).T;
        coordTL = [coordTL;...
            -xLimits(1)+tFormsMatrix(3,1),-yLimits(1)+tFormsMatrix(3,2)];
        
        TLtoCent_OS = [camW/2, camH/2]*tFormsMatrix(1:2,1:2);
        coordCent = [coordCent;...
            coordTL(i,1)+TLtoCent_OS(1),coordTL(i,2)+TLtoCent_OS(2)];
        
        TLtoTR_OS = [camW, 0]*tFormsMatrix(1:2,1:2);
        coordTR = [coordTR;...
            coordTL(i,1)+TLtoTR_OS(1),coordTL(i,2)+TLtoTR_OS(2)];
        
        TLtoBL_OS = [0, camH]*tFormsMatrix(1:2,1:2);
        coordBL = [coordBL;...
            coordTL(i,1)+TLtoBL_OS(1),coordTL(i,2)+TLtoBL_OS(2)];
        
        TLtoBR_OS = [camW, camH]*tFormsMatrix(1:2,1:2);
        coordBR = [coordBR;...
            coordTL(i,1)+TLtoBR_OS(1),coordTL(i,2)+TLtoBR_OS(2)];
        
        % Generate a binary mask.    
        mask = imwarp(true(size(I,1),size(I,2)), tforms(i), 'OutputView', panoramaView);

        % Overlay the warpedImage onto the panorama.
        disp(i);
        panorama = step(blender, panorama, warpedImage, mask);
        partPanoramas = cat(3,partPanoramas,panorama);
        
        rgbPanorama = cat(3, panorama, panorama, panorama);
        pW = 35;
        pW2 = 15;
        pT = 2;
        %origin (green)
        x = round(coordCent(1,1));
        y = round(coordCent(1,2));
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 1) = 0;
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 2) = 255;
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 3) = 0;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 1) = 0;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 2) = 255;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 3) = 0;
        rgbPanorama = insertShape(rgbPanorama,'Polygon',...
            [coordTL(1,1) coordTL(1,2) coordTR(1,1) coordTR(1,2)...
            coordBR(1,1) coordBR(1,2) coordBL(1,1) coordBL(1,2) ],...
            'LineWidth',4,'Color','g');
        %previous frame (red)
        if i>1
            x = round(coordCent(i-1,1));
            y = round(coordCent(i-1,2));
            rgbPanorama(y-pT:y+pT, x-pW2:x+pW2, 1) = 255;
            rgbPanorama(y-pT:y+pT, x-pW2:x+pW2, 2) = 0;
            rgbPanorama(y-pT:y+pT, x-pW2:x+pW2, 3) = 0;
            rgbPanorama(y-pW2:y+pW2, x-pT:x+pT, 1) = 255;
            rgbPanorama(y-pW2:y+pW2, x-pT:x+pT, 2) = 0;
            rgbPanorama(y-pW2:y+pW2, x-pT:x+pT, 3) = 0;
        end
        %current frame (cyan)
        x = round(coordCent(i,1));
        y = round(coordCent(i,2));
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 1) = 0;
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 2) = 255;
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 3) = 255;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 1) = 0;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 2) = 255;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 3) = 255;
        rgbPanorama = insertShape(rgbPanorama,'Polygon',...
        [coordTL(i,1) coordTL(i,2) coordTR(i,1) coordTR(i,2)...
        coordBR(i,1) coordBR(i,2) coordBL(i,1) coordBL(i,2) ],...
        'LineWidth',4,'Color','c');
        
        %calculations
        xPosPix = [xPosPix; coordCent(i,1) - coordCent(1,1)];        
        yPosPix = [yPosPix; coordCent(1,2) - coordCent(i,2)];
        distPix = [distPix;sqrt(xPosPix(i)^2+yPosPix(i)^2)];
        if i > 1
           xVelPix = [xVelPix; (coordCent(i,1)-coordCent(i-1,1))/(1/6)];
           yVelPix = [yVelPix; (coordCent(i,2)-coordCent(i-1,2))/(1/6)];
           velPix = [velPix;sqrt(xVelPix(i)^2+yVelPix(i)^2)];
        end
        
        rise = coordBL(i,2)-coordTL(i,2);
        run = coordTL(i,1)-coordBL(i,1);
        if (rise>=0 && run>=0) || (rise < 0 && run >= 0)
            angle = [angle; atan(rise/run)*180/pi-90];        
        elseif (rise >= 0 && run < 0) || (rise < 0 && run < 0)
            angle = [angle; atan(rise/run)*180/pi+90];
        end
        
        leftL_I = sqrt( (coordBL(1,2)-coordTL(1,2))^2 +...
        (coordTL(1,1) - coordBL(1,1))^2);
        leftL = sqrt( (coordBL(i,2)-coordTL(i,2))^2 +...
            (coordTL(i,1) - coordBL(i,1))^2);
        leftLScale = [leftLScale; leftL/leftL_I];
        
        
        topL_I = sqrt( (coordTR(1,2)-coordTL(1,2))^2 +...
            (coordTL(1,1) - coordTR(1,1))^2);
        topL = sqrt( (coordTR(i,2)-coordTL(i,2))^2 +...
            (coordTL(i,1) - coordTR(i,1))^2);
        topLScale = [topLScale; topL/topL_I];
        
        %text on Panorama
        position = [round(width*.5),round(height*.05)];
        text = 'Pos (pix/sec): ' + string(round(xPosPix(i))) +...
            ', ' + string(round(yPosPix(i))) + newline...
        + 'Vel (pix/sec): ' + string(round(velPix(i))) + newline...   
        + 'Ang (deg): ' + string(round(angle(i))) + newline...
        + 'LeftScale: ' + string(round(leftLScale(i),2)) + newline...
        + 'TopScale: ' + string(round(topLScale(i),2));
        rgbPanorama = insertText(rgbPanorama,position,text,...
            'FontSize',50,'BoxColor', 'w','BoxOpacity',1,'TextColor','black');
        
        %partial panoramas
        markedPartPanoramas = cat(4,markedPartPanoramas,rgbPanorama);
    end

    figure('Name','Panorama')
    imshow(panorama)

%PLAY VIDEO
    %https://www.mathworks.com/help/images/ref/videoviewer-app.html
%     implay(testImgs,6)
%     implay(partPanoramas,6)
     implay(markedPartPanoramas,6)
    
%putting points on the partial panorama
        %https://www.mathworks.com/matlabcentral/answers/269694-how-to-plot-some-points-on-an-image
%         i = 1;
%         
%         figure();
%         imshow(partPanoramas(:,:,i));
%         hold on;
%         %origin
%         plot(coordCent(1,1),coordCent(1,2),'g+');
%         plot([coordTL(1,1) coordTR(1,1)], [coordTL(1,2) coordTR(1,2)],'g');
%         plot([coordTR(1,1) coordBR(1,1)], [coordTR(1,2) coordBR(1,2)],'g');
%         plot([coordBR(1,1) coordBL(1,1)], [coordBR(1,2) coordBL(1,2)],'g');
%         plot([coordBL(1,1) coordTL(1,1)], [coordBL(1,2) coordTL(1,2)],'g');
%         %previous frame center
%         if i > 1
%             plot(coordCent(i-1,1),coordCent(i-1,2),'r+');
%         end
%         %current frame
%         plot(coordCent(i,1),coordCent(i,2),'c+');
%         plot([coordTL(i,1) coordTR(i,1)], [coordTL(i,2) coordTR(i,2)],'c');
%         plot([coordTR(i,1) coordBR(i,1)], [coordTR(i,2) coordBR(i,2)],'c');
%         plot([coordBR(i,1) coordBL(i,1)], [coordBR(i,2) coordBL(i,2)],'c');
%         plot([coordBL(i,1) coordTL(i,1)], [coordBL(i,2) coordTL(i,2)],'c');
        
%arivnd says remove blender for testing

% Implement adapthisteq to increase contrast of gray image for features
% Use optimization algorithm to improve image alignment
% Will optimization improve scale when passing back over visited areas?
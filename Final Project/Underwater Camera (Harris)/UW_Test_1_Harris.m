%EECE 5554 Final AR SLAM Test Code 4

clear all
close all

% Load Images
muralDir = fullfile('6Images/','*');
muralScene = imageDatastore(muralDir);

% Display Images
figure(1)
montage(muralScene.Files,'Size',[3 2])

testLength = 6;

%LAB 5 HARRIS CORNER METHOD
    % Changes made:
    % combination of lab5 code and IR_Test_4_SIFT code
    % adapthisteq: cliplimit 0.02, distribution rayleigh, alpha 0.4
    % harris change: 1000 to 2000, [2 2] to [4 4]
    % remove camera distortion corrections
    % **** After successful Mosaic creation:
    % tforms increase confidence from 99.9 to 99.99
    % adapthisteq: cliplimit from 0.02 to 0.05, alpha 0.4 to 0.7
    % harris: points 4000 to 2000

    % Read the first image from the image set.
    I = readimage(muralScene,1);

    % Initialize features for I(1)
    Ihist = adapthisteq(I,'NumTiles',[4 4],'clipLimit',0.05,'Distribution','rayleigh','Alpha',0.7);
    grayImage = im2gray(Ihist);
    [y,x,m] = harris_mod(grayImage,false,2000,'tile',[4 4],'disp');
    points = [x,y];
    [features, points] = extractFeatures(grayImage,points);

    % Initialize all the transforms to the identity matrix.
    numImages = testLength;
    tforms(numImages) = affine2d(eye(3)); % projective or affine

    % Initialize variable to hold image sizes.
    imageSize = zeros(numImages,2);

    % Iterate over remaining image pairs
    for n = 2:numImages

        % Store points and features for I(n-1).
        pointsPrevious = points;
        featuresPrevious = features;

        % Read I(n).
        I = readimage(muralScene,n);
        I = imhistmatch(I,readimage(muralScene,n-1));

        % Convert image to grayscale.
        Ihist = adapthisteq(I,'NumTiles',[4 4],'clipLimit',0.05,'Distribution','rayleigh','Alpha',0.7);
        grayImage = im2gray(Ihist);

        % Save image size.
        imageSize(n,:) = size(grayImage);

        % Detect and extract Harris.
        [y,x,m] = harris_mod(grayImage,false,2000,'tile',[4 4],'disp');
        points = [x,y];
        [features, points] = extractFeatures(grayImage,points);

        % Find correspondences between I(n) and I(n-1).
        indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);

        matchedPoints = points(indexPairs(:,1), :);
        matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);        

        % Estimate the transformation between I(n) and I(n-1).
        tforms(n) = estimateGeometricTransform2D(matchedPoints, matchedPointsPrev,...
            'affine', 'Confidence', 99.99, 'MaxNumTrials', 2000);

        % Compute T(n) * T(n-1) * ... * T(1)
        tforms(n).T = tforms(n).T * tforms(n-1).T; 
    end

    % Compute the output limits for each transform.
for i = 1:numel(tforms)           
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);    
end

% Find center image
avgXLim = mean(xlim, 2);
[~,idx] = sort(avgXLim);
centerIdx = floor((numel(tforms)+1)/2);
centerImageIdx = idx(centerIdx);

% Apply transform img inverse transform to others
Tinv = invert(tforms(centerImageIdx));
for i = 1:numel(tforms)    
    tforms(i).T = tforms(i).T * Tinv.T;
end

% Initialize panorama
for i = 1:numel(tforms)           
    [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);
end

maxImageSize = max(imageSize);

% Find the minimum and maximum output limits. 
xMin = min([1; xlim(:)]);
xMax = max([maxImageSize(2); xlim(:)]);

yMin = min([1; ylim(:)]);
yMax = max([maxImageSize(1); ylim(:)]);

% Width and height of panorama.
width  = round(xMax - xMin);
height = round(yMax - yMin);

% Initialize the "empty" panorama.
panorama = zeros([height width], 'like', I);

% Create panorama
blender = vision.AlphaBlender('Operation', 'Binary mask', ...
    'MaskSource', 'Input port');  

% Create a 2-D spatial reference object defining the size of the panorama.
xLimits = [xMin xMax];
yLimits = [yMin yMax];
panoramaView = imref2d([height width], xLimits, yLimits);

% Create the panorama.
for i = 1:numImages
    
    I = readimage(muralScene, i);
    if i > 1
        I = imhistmatch(I,readimage(muralScene,i-1));
    end
   
    % Transform I into the panorama.
    warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);
                  
    % Generate a binary mask.    
    mask = imwarp(true(size(I,1),size(I,2)), tforms(i), 'OutputView', panoramaView);
    
    % Overlay the warpedImage onto the panorama.
    panorama = step(blender, panorama, warpedImage, mask);
end

figure('Name','Panorama')
imshow(panorama)
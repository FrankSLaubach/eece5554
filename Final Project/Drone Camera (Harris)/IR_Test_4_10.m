%EECE 5554 Final AR SLAM Test Code 4_9
clear all
close all

%READ DATA
    bagPath = "uas4_images_2019-07-20-15-45-32_filtered-003.bag";
    bagSelect = rosbag(bagPath);
    bagInfo = rosbag('info',bagPath);

    cameraInfoTopic = select(bagSelect,'Topic','uas4/camera_info');
    cameraInfoRead = readMessages(cameraInfoTopic,'DataFormat','struct');

    imageRawTopic = select(bagSelect,'Topic','uas4/image_raw');
    imageRawRead = readMessages(imageRawTopic,'DataFormat','struct');

 %FIX DISTORTION
    %https://www.mathworks.com/help/ros/ref/readimage.html
    distortedImg = rosReadImage(imageRawRead{1});
    %https://www.mathworks.com/help/vision/ref/undistortimage.html
    IntrinsicMatrix = [ 524.888150200348832 0                   0; ...
                        0                   521.776791343664968 0; ...
                        325.596989785447420 242.392342491041603 1];
    radialDistortion = [-0.470302508060718438   0.301057860458473880]; 
    tangentialDistortion = [0.00468835914496582538	0.00165573977268025185];
    imageSize = [512,640];
    cameraParams = cameraParameters('IntrinsicMatrix',IntrinsicMatrix,...
        'RadialDistortion',radialDistortion,...
        'TangentialDistortion',tangentialDistortion,...
        'ImageSize',imageSize); 
    [undistortedImg, origin] = undistortImage(distortedImg,cameraParams);

%LOAD IMAGES
    %https://www.mathworks.com/help/matlab/math/multidimensional-arrays.html
    testLength = 156; %max is 700. try 360, 156, 180, 200
    testImgs = [];
    for i = 1:10:testLength*10
       image = rosReadImage(imageRawRead{i});
       [image, origin] = undistortImage(image,cameraParams);
       testImgs = cat(3,testImgs,image);
    end
    %figure('Name','Montage');
    %montage(testImgs);
    
%LAB 5 HARRIS CORNER METHOD
    % Read the first image from the image set.
    I = testImgs(:,:,1);

    % Initialize features for I(1)
    grayImage = im2gray(I);
    [y,x,m] = harris_mod(grayImage,false,1000,'tile',[2 2],'disp');
    points = [x,y];
    [features, points] = extractFeatures(grayImage,points);
    
    % Initialize all the transforms to the identity matrix. Note that the
    % projective transform is used here because the building images are fairly
    % close to the camera. Had the scene been captured from a further distance,
    % an affine transform would suffice.
    numImages = testLength;
    tforms(numImages) = affine2d(eye(3));

    % Initialize variable to hold image sizes.
    imageSize = zeros(numImages,2);
    
    %ADD: Panorama Array
    camW = cameraParams.ImageSize(2);
    camH = cameraParams.ImageSize(1);
    
    markedPartPanoramas = [];
    xPosPix = [];
    yPosPix = [];
    distPix = [];
    xVelPix = [0];
    yVelPix = [0];    
    velPix = [0];
    angle = [];    
    leftLScale = [];
    topLScale = [];
    
    %first image in panorama
    % Find the minimum and maximum output limits. 
    xMin = -camW*(0.5+10);
    xMax = camW*(1.5+10);

    yMin = -camH*(0.5+10);
    yMax = camH*(1.5+10);
    
    coordTL = [-xMin,-yMin];
    
    TLtoCent_OS = [camW/2, camH/2];
    coordCent = [coordTL(1)+TLtoCent_OS(1),coordTL(2)+TLtoCent_OS(2)];
    
    TLtoTR_OS = [camW, 0];
    coordTR = [coordTL(1)+TLtoTR_OS(1),coordTL(2)+TLtoTR_OS(2)];

    TLtoBL_OS = [0, camH];
    coordBL = [coordTL(1)+TLtoBL_OS(1),coordTL(2)+TLtoBL_OS(2)];

    TLtoBR_OS = [camW, camH];
    coordBR = [coordTL(1)+TLtoBR_OS(1),coordTL(2)+TLtoBR_OS(2)];
    
    % Width and height of panorama.
    width  = round(xMax - xMin);
    height = round(yMax - yMin);

    % Initialize the "empty" panorama.
    panorama = zeros([height width], 'like', I);

    blender = vision.AlphaBlender('Operation', 'Binary mask', ...
        'MaskSource', 'Input port');  

    % Create a 2-D spatial reference object defining the size of the panorama.    
    xLimits = [xMin xMax];
    yLimits = [yMin yMax];
    panoramaView = imref2d([height width], xLimits, yLimits);
    
    %create image
    warpedImage = imwarp(I, tforms(1), 'OutputView', panoramaView);    
    mask = imwarp(true(size(I,1),size(I,2)), tforms(1), 'OutputView', panoramaView);
    panorama = step(blender, panorama, warpedImage, mask);
    partPanoramas = panorama;
    panoPoints = [];
    
    %panorama origin
    oPointX = -xMin;
    oPointY = -yMin;  
    
    % Create the panorama.
    for i = 2:6
        %translate previous (i-1) points onto panorama
        tFormsMatrix = tforms(i-1).T;
        points = points+coordTL(i-1,:);
        for p = 1:size(points,1)            
           pOS = [points(p,1)-coordTL(i-1,1), points(p,2)-coordTL(i-1,2)]*...
               tFormsMatrix(1:2,1:2);
           points(p,:) = coordTL(i-1,:) + pOS;
        end
        %panoPoints = [panoPoints;points];
        
        %extract features from previous points
        %instead of extracting from whole panorama, only subsection
        LeftBound = 0;
        RightBound = 0;
        TopBound = 0;
        BotBound = 0;
        Buffer = 200;%pixels past the bounds
        if coordTL(i-1,1) <= coordBL(i-1,1)
            LeftBound = coordTL(i-1,1)-Buffer;
        elseif coordBL(i-1,1) < coordTL(i-1,1)
            LeftBound = coordBL(i-1,1)-Buffer;            
        end
        if coordTR(i-1,1) >= coordBR(i-1,1)
            RightBound = coordTR(i-1,1)+Buffer;
        elseif coordBR(i-1,1) > coordTR(i-1,1)
            RightBound = coordBR(i-1,1)+Buffer;            
        end
        if coordTL(i-1,2) <= coordTR(i-1,2)
            TopBound = coordTL(i-1,2)-Buffer;
        elseif coordTR(i-1,2) < coordTL(i-1,2)
            TopBound = coordTR(i-1,2)-Buffer;            
        end
        if coordBL(i-1,2) >= coordBR(i-1,2)
            BotBound = coordBL(i-1,2)+Buffer;
        elseif coordBR(i-1,2) > coordBL(i-1,2)
            BotBound = coordBR(i-1,2)+Buffer;            
        end
        panoPointsSub = [];
        for g = 1:size(panoPoints,1)
            x = panoPoints(g,1);
            y = panoPoints(g,2);
            if x >LeftBound && x < RightBound ...
                    && y > TopBound && y < BotBound
               panoPointsSub = [panoPointsSub;panoPoints(g,:)]; 
            end
        end    
        
        %cut away the image (i-1) from the subsection
        %https://www.mathworks.com/help/matlab/ref/inpolygon.html
        xv = [coordTL(i-1,1) coordTR(i-1,1) coordBR(i-1,1) coordBL(i-1,1) coordTL(i-1,1)];
        yv = [coordTL(i-1,2) coordTR(i-1,2) coordBR(i-1,2) coordBL(i-1,2) coordTL(i-1,2)];
        badPoints = [];
        for g = 1:size(panoPointsSub,1)
            x = panoPointsSub(g,1);
            y = panoPointsSub(g,2);
            if inpolygon(x,y,xv,yv) 
                badPoints = [badPoints;g];
            end
        end
        for g=badPoints
           panoPointsSub(g,:) = [];
        end
        
        pointsFocus = [points;panoPointsSub];
        [featuresPrevious, pointsPrevious] =...
            extractFeatures(panorama,pointsFocus);
        figure('Name', 'Harris Points for: '+string(i)); imshow(panorama); 
        hold on; plot(pointsFocus(:,1),pointsFocus(:,2),'y+');
       
        % Harris points for current image
        I = testImgs(:,:,i);
        grayImage = im2gray(I);    
        imageSize(i,:) = size(grayImage);
        [y,x,m] = harris_mod(grayImage,false,1000,'tile',[2 2],'disp');%size(pointsPrevious,1) also tried
        points = [x,y];
        [features, points] = extractFeatures(grayImage,points);

        % Find correspondences between image and panorama
        indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);
        matchedPoints = points(indexPairs(:,1), :);
        matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);   
        panoPoints = [panoPoints;matchedPointsPrev];
        panoPoints = unique(panoPoints,'rows');
        %panoPoints = setdiff(panoPoints,matchedPointsPrev,'rows');
        
        % Estimate the transformation between image and panorama subsection
        tforms(i) = estimateGeometricTransform2D(matchedPoints, matchedPointsPrev,...
            'affine', 'Confidence', 99.9, 'MaxNumTrials', 4000);      
        tforms(i).T(3,1) = tforms(i).T(3,1)-oPointX;
        tforms(i).T(3,2) = tforms(i).T(3,2)-oPointY;
        
        %Collect coordinates of corners and center
        %note that panorama center is at -xLimits(1), -yLimits(1)
        %note that MATLAB coords are measured from the top left corner
        tFormsMatrix = tforms(i).T;
        coordTL = [coordTL;tFormsMatrix(3,1)+oPointX,...
            tFormsMatrix(3,2)+oPointY];
        
        TLtoCent_OS = [camW/2, camH/2]*tFormsMatrix(1:2,1:2);
        coordCent = [coordCent;...
            coordTL(i,1)+TLtoCent_OS(1),coordTL(i,2)+TLtoCent_OS(2)];
        
        TLtoTR_OS = [camW, 0]*tFormsMatrix(1:2,1:2);
        coordTR = [coordTR;...
            coordTL(i,1)+TLtoTR_OS(1),coordTL(i,2)+TLtoTR_OS(2)];
        
        TLtoBL_OS = [0, camH]*tFormsMatrix(1:2,1:2);
        coordBL = [coordBL;...
            coordTL(i,1)+TLtoBL_OS(1),coordTL(i,2)+TLtoBL_OS(2)];
        
        TLtoBR_OS = [camW, camH]*tFormsMatrix(1:2,1:2);
        coordBR = [coordBR;...
            coordTL(i,1)+TLtoBR_OS(1),coordTL(i,2)+TLtoBR_OS(2)];
        
        % Transform I into the panorama.
        warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);     
                
        % Generate a binary mask.    
        mask = imwarp(true(size(I,1),size(I,2)), tforms(i), 'OutputView', panoramaView);

        % Overlay the warpedImage onto the panorama.
        disp(i);
        panorama = step(blender, panorama, warpedImage, mask);        
        partPanoramas = cat(3,partPanoramas,panorama);
        
        rgbPanorama = cat(3, panorama, panorama, panorama);
        pW = 35;
        pW2 = 15;
        pT = 2;
        %origin (green)
        x = round(coordCent(1,1));
        y = round(coordCent(1,2));
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 1) = 0;
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 2) = 255;
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 3) = 0;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 1) = 0;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 2) = 255;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 3) = 0;
        rgbPanorama = insertShape(rgbPanorama,'Polygon',...
            [coordTL(1,1) coordTL(1,2) coordTR(1,1) coordTR(1,2)...
            coordBR(1,1) coordBR(1,2) coordBL(1,1) coordBL(1,2) ],...
            'LineWidth',4,'Color','g');
        %previous frame (red)
        if i>1
            x = round(coordCent(i-1,1));
            y = round(coordCent(i-1,2));
            rgbPanorama(y-pT:y+pT, x-pW2:x+pW2, 1) = 255;
            rgbPanorama(y-pT:y+pT, x-pW2:x+pW2, 2) = 0;
            rgbPanorama(y-pT:y+pT, x-pW2:x+pW2, 3) = 0;
            rgbPanorama(y-pW2:y+pW2, x-pT:x+pT, 1) = 255;
            rgbPanorama(y-pW2:y+pW2, x-pT:x+pT, 2) = 0;
            rgbPanorama(y-pW2:y+pW2, x-pT:x+pT, 3) = 0;
        end
        %current frame (cyan)
        x = round(coordCent(i,1));
        y = round(coordCent(i,2));
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 1) = 0;
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 2) = 255;
        rgbPanorama(y-pT:y+pT, x-pW:x+pW, 3) = 255;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 1) = 0;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 2) = 255;
        rgbPanorama(y-pW:y+pW, x-pT:x+pT, 3) = 255;
        rgbPanorama = insertShape(rgbPanorama,'Polygon',...
        [coordTL(i,1) coordTL(i,2) coordTR(i,1) coordTR(i,2)...
        coordBR(i,1) coordBR(i,2) coordBL(i,1) coordBL(i,2) ],...
        'LineWidth',4,'Color','c');
    
        %partial panoramas
        markedPartPanoramas = cat(4,markedPartPanoramas,rgbPanorama);
        figure('Name', 'Part Panorama for: '+string(i));
        imshow(rgbPanorama);               
    end
    
    
figure();        
imshow(panorama);

implay(partPanoramas,6)
Versions:
IR Test 1: Panorama
IR Test 2: Partial Panorama montage
IR Test 3: Partial Panoramas with center and corners marked
IR Test 4: Partial Panorama montage with center and corners marked, info displayed

IR Test 4_1: Trying to do 4_0 with SIFT

IR Test 4_2: compare current image with panorama instead of only previous image
	-tf is now calculated in the main loop, relative to the mosaic instead of the previous image
	-current image compared with subsection of panorama instead of entire panorama
	-panorama changes sizes every iteration
IR Test 4_3: compare current image with panorama instead of only previous image
	-but panorama set to a constant size
IR Test 4_4: compare current image with panorama instead of only previous image
	-but with code to delete points near edge of image
IR Test 4_5: compare current image with panorama instead of only previous image
	-but with code to delete all points not on white pavement lines

IR Test 4_7: Trying to do 4_0 with white pavement lines

IR Test 4_6: save all previous points, use tf to plot on panorama
IR Test 4_8: save all previous points, use tf to plot on panorama
	-tried only white pavement
IR Test 4_9: save all previous points, use tf to plot on panorama
	-tried only looking at subsection of panorama points
IR Test 4_10: save all previous points, use tf to plot on panorama
	-only looking at subsection of panorama points
	-comparison between current image and previous image SUPPLEMENTED by panorama points
	-removed overlap between 2 point sets
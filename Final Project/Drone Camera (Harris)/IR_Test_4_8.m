%EECE 5554 Final AR SLAM Test Code 4_8
clear all
close all

%READ DATA
    bagPath = "uas4_images_2019-07-20-15-45-32_filtered-003.bag";
    bagSelect = rosbag(bagPath);
    bagInfo = rosbag('info',bagPath);

    cameraInfoTopic = select(bagSelect,'Topic','uas4/camera_info');
    cameraInfoRead = readMessages(cameraInfoTopic,'DataFormat','struct');

    imageRawTopic = select(bagSelect,'Topic','uas4/image_raw');
    imageRawRead = readMessages(imageRawTopic,'DataFormat','struct');

 %FIX DISTORTION
    %https://www.mathworks.com/help/ros/ref/readimage.html
    distortedImg = rosReadImage(imageRawRead{1});
    %https://www.mathworks.com/help/vision/ref/undistortimage.html
    IntrinsicMatrix = [ 524.888150200348832 0                   0; ...
                        0                   521.776791343664968 0; ...
                        325.596989785447420 242.392342491041603 1];
    radialDistortion = [-0.470302508060718438   0.301057860458473880]; 
    tangentialDistortion = [0.00468835914496582538	0.00165573977268025185];
    imageSize = [512,640];
    cameraParams = cameraParameters('IntrinsicMatrix',IntrinsicMatrix,...
        'RadialDistortion',radialDistortion,...
        'TangentialDistortion',tangentialDistortion,...
        'ImageSize',imageSize); 
    [undistortedImg, origin] = undistortImage(distortedImg,cameraParams);

%LOAD IMAGES
    %https://www.mathworks.com/help/matlab/math/multidimensional-arrays.html
    testLength = 156; %max is 700. try 360, 156, 180, 200
    testImgs = [];
    for i = 1:10:testLength*10
       image = rosReadImage(imageRawRead{i});
       [image, origin] = undistortImage(image,cameraParams);
       testImgs = cat(3,testImgs,image);
    end
    %figure('Name','Montage');
    %montage(testImgs);
    
%LAB 5 HARRIS CORNER METHOD
    % Read the first image from the image set.
    I = testImgs(:,:,1);

    % Initialize features for I(1)
    grayImage = im2gray(I);
    [y,x,m] = harris_mod(grayImage,false,1000,'tile',[2 2],'disp');
    points = [x,y];
%     %make sure the points are on white spots
%     threshold = 180;
%     badPoints = [];
%     for n = 1:size(points,1)
%         x = points(n,1);
%         y = points(n,2);
%         valid = 0;
%         if grayImage(y,x)>threshold
%             valid = 1;
%         end
%         if valid==0
%            badPoints = [badPoints;n];               
%         end
%     end        
%     for n=badPoints
%         points(n,:) = [];
%     end
    [features, points] = extractFeatures(grayImage,points);
    
    % Initialize all the transforms to the identity matrix. Note that the
    % projective transform is used here because the building images are fairly
    % close to the camera. Had the scene been captured from a further distance,
    % an affine transform would suffice.
    numImages = testLength;
    tforms(numImages) = affine2d(eye(3));

    % Initialize variable to hold image sizes.
    imageSize = zeros(numImages,2);
    
    %ADD: Panorama Array
    camW = cameraParams.ImageSize(2);
    camH = cameraParams.ImageSize(1);
    
    coordTR = [];
    coordBL = [];
    coordBR = [];
    markedPartPanoramas = [];
    xPosPix = [];
    yPosPix = [];
    distPix = [];
    xVelPix = [0];
    yVelPix = [0];    
    velPix = [0];
    angle = [];    
    leftLScale = [];
    topLScale = [];
    
    %first image in panorama
    % Find the minimum and maximum output limits. 
    xMin = -camW*(0.5+10);
    xMax = camW*(1.5+10);

    yMin = -camH*(0.5+10);
    yMax = camH*(1.5+10);
    
    coordTL = [-xMin,-yMin];
    TLtoCent_OS = [camW/2, camH/2];
    coordCent = [coordTL(1)+TLtoCent_OS(1),coordTL(2)+TLtoCent_OS(2)];
    
    % Width and height of panorama.
    width  = round(xMax - xMin);
    height = round(yMax - yMin);

    % Initialize the "empty" panorama.
    panorama = zeros([height width], 'like', I);

    blender = vision.AlphaBlender('Operation', 'Binary mask', ...
        'MaskSource', 'Input port');  

    % Create a 2-D spatial reference object defining the size of the panorama.    
    xLimits = [xMin xMax];
    yLimits = [yMin yMax];
    panoramaView = imref2d([height width], xLimits, yLimits);
    
    %create image
    warpedImage = imwarp(I, tforms(1), 'OutputView', panoramaView);    
    mask = imwarp(true(size(I,1),size(I,2)), tforms(1), 'OutputView', panoramaView);
    panorama = step(blender, panorama, warpedImage, mask);
    partPanoramas = panorama;
    panoPoints = [];
    
    %panorama origin
    oPointX = -xMin;
    oPointY = -yMin;  
    
    % Create the panorama.
    for i = 2:15
        %translate previous (i-1) points onto panorama
        tFormsMatrix = tforms(i-1).T;
        points = points+coordTL(i-1,:);
        for p = 1:size(points,1)            
           pOS = [points(p,1)-coordTL(i-1,1), points(p,2)-coordTL(i-1,2)]*...
               tFormsMatrix(1:2,1:2);
           points(p,:) = coordTL(i-1,:) + pOS;
        end
        panoPoints = [panoPoints;points];
        [featuresPrevious, pointsPrevious] =...
            extractFeatures(panorama,panoPoints);
        figure(); imshow(panorama); hold on; plot(panoPoints(:,1),panoPoints(:,2),'y+');
       
        % Harris points for current image
        I = testImgs(:,:,i);
        grayImage = im2gray(I);    
        imageSize(i,:) = size(grayImage);
        [y,x,m] = harris_mod(grayImage,false,1000,'tile',[2 2],'disp');
        points = [x,y];
%         %make sure the points are on white spots
%         threshold = 180;
%         badPoints = [];
%         for n = 1:size(points,1)
%             x = points(n,1);
%             y = points(n,2);
%             valid = 0;
%             if grayImage(y,x)>threshold
%                 valid = 1;
%             end
%             if valid==0
%                badPoints = [badPoints;n];               
%             end
%         end        
%         for n=badPoints
%             points(n,:) = [];
%         end
        [features, points] = extractFeatures(grayImage,points);

        % Find correspondences between image and panorama
        indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);
        matchedPoints = points(indexPairs(:,1), :);
        matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);        
        panoPoints = setdiff(panoPoints,matchedPointsPrev,'rows');
        
        % Estimate the transformation between image and panorama subsection
        tforms(i) = estimateGeometricTransform2D(matchedPoints, matchedPointsPrev,...
            'affine', 'Confidence', 99, 'MaxNumTrials', 2000);      
        tforms(i).T(3,1) = tforms(i).T(3,1)-oPointX;
        tforms(i).T(3,2) = tforms(i).T(3,2)-oPointY;
        
        %Collect coordinates of corners and center
        %note that panorama center is at -xLimits(1), -yLimits(1)
        %note that MATLAB coords are measured from the top left corner
        tFormsMatrix = tforms(i).T;
        coordTL = [coordTL;tFormsMatrix(3,1)+oPointX,...
            tFormsMatrix(3,2)+oPointY];
        TLtoCent_OS = [camW/2, camH/2]*tFormsMatrix(1:2,1:2);
        coordCent = [coordCent;...
            coordTL(i,1)+TLtoCent_OS(1),coordTL(i,2)+TLtoCent_OS(2)];
        
        % Transform I into the panorama.
        warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);     
                
        % Generate a binary mask.    
        mask = imwarp(true(size(I,1),size(I,2)), tforms(i), 'OutputView', panoramaView);

        % Overlay the warpedImage onto the panorama.
        disp(i);
        panorama = step(blender, panorama, warpedImage, mask);        
        partPanoramas = cat(3,partPanoramas,panorama);
%         figure();
%         imshow(warpedImage);
        
        figure();        
        imshow(panorama);
        hold on;
        %origin
        plot(coordCent(1,1),coordCent(1,2),'r+');
        plot(coordTL(1,1),coordTL(1,2),'r+');
        %current frame
        plot(coordCent(i,1),coordCent(i,2),'c+');
        plot(coordTL(i,1),coordTL(i,2),'c+');
                       
    end
    
    
figure();        
imshow(panorama);

implay(partPanoramas,6)
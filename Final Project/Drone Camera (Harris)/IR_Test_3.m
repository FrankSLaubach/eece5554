%EECE 5554 Final AR SLAM Test Code 3

clear all
close all

%READ DATA
    bagPath = "uas4_images_2019-07-20-15-45-32_filtered-003.bag";
    bagSelect = rosbag(bagPath);
    bagInfo = rosbag('info',bagPath);

    cameraInfoTopic = select(bagSelect,'Topic','uas4/camera_info');
    cameraInfoRead = readMessages(cameraInfoTopic,'DataFormat','struct');
    cameraInfoRead{1};

    imageRawTopic = select(bagSelect,'Topic','uas4/image_raw');
    imageRawRead = readMessages(imageRawTopic,'DataFormat','struct');
    imageRawRead{1};

 %FIX DISTORTION
    %https://www.mathworks.com/help/ros/ref/readimage.html
    distortedImg = rosReadImage(imageRawRead{1});

    %https://www.mathworks.com/help/vision/ref/undistortimage.html
    IntrinsicMatrix = [ 524.888150200348832 0                   0; ...
                        0                   521.776791343664968 0; ...
                        325.596989785447420 242.392342491041603 1];
    radialDistortion = [-0.470302508060718438   0.301057860458473880]; 
    tangentialDistortion = [0.00468835914496582538	0.00165573977268025185];
    imageSize = [512,640];
    cameraParams = cameraParameters('IntrinsicMatrix',IntrinsicMatrix,...
        'RadialDistortion',radialDistortion,...
        'TangentialDistortion',tangentialDistortion,...
        'ImageSize',imageSize); 
    [undistortedImg, origin] = undistortImage(distortedImg,cameraParams);

    figure('Name','Remove Distortion Ex');
    montage(cat(3,distortedImg,undistortedImg));

%LOAD IMAGES
    %https://www.mathworks.com/help/matlab/math/multidimensional-arrays.html
    testLength = 150;
    testImgs = [];
    for i = 1:10:testLength*10
       image = rosReadImage(imageRawRead{i});
       [image, origin] = undistortImage(image,cameraParams);
       testImgs = cat(3,testImgs,image);
    end
    figure('Name','Montage');
    montage(testImgs);

%LAB 5 HARRIS CORNER METHOD
    %changes made:
    %I = testImgs(:,:,1);
    %numImages = testLength;
    %tforms(numImages) = affine2d(eye(3));
    %panorama = zeros([height width 3], 'like', I); %removed 3 for grayscale
    %harris changed to harris_mod (boolean for show image)
    %Panorama array
    %Points for the panorama array

    % Read the first image from the image set.
    I = testImgs(:,:,1);

    % Initialize features for I(1)
    grayImage = im2gray(I);
    [y,x,m] = harris_mod(grayImage,true,1000,'tile',[2 2],'disp');
    points = [x,y];
    [features, points] = extractFeatures(grayImage,points);

    % Initialize all the transforms to the identity matrix. Note that the
    % projective transform is used here because the building images are fairly
    % close to the camera. Had the scene been captured from a further distance,
    % an affine transform would suffice.
    numImages = testLength;
    tforms(numImages) = affine2d(eye(3));

    % Initialize variable to hold image sizes.
    imageSize = zeros(numImages,2);

    % Iterate over remaining image pairs
    for n = 2:numImages

        % Store points and features for I(n-1).
        pointsPrevious = points;
        featuresPrevious = features;

        % Read I(n).
        I = testImgs(:,:,n);

        % Convert image to grayscale.
        grayImage = im2gray(I);    

        % Save image size.
        imageSize(n,:) = size(grayImage);

        % Detect and extract Harris.
        [y,x,m] = harris_mod(grayImage,false,1000,'tile',[2 2],'disp');
        points = [x,y];
        [features, points] = extractFeatures(grayImage,points);

        % Find correspondences between I(n) and I(n-1).
        indexPairs = matchFeatures(features, featuresPrevious, 'Unique', true);

        matchedPoints = points(indexPairs(:,1), :);
        matchedPointsPrev = pointsPrevious(indexPairs(:,2), :);        

        % Estimate the transformation between I(n) and I(n-1).
        tforms(n) = estimateGeometricTransform2D(matchedPoints, matchedPointsPrev,...
            'affine', 'Confidence', 99.9, 'MaxNumTrials', 2000);

        % Compute T(n) * T(n-1) * ... * T(1)
        tforms(n).T = tforms(n).T * tforms(n-1).T; 
    end

    % Compute the output limits for each transform.
    for i = 1:numel(tforms)           
        [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);    
    end

    avgXLim = mean(xlim, 2);
    [~,idx] = sort(avgXLim);
    centerIdx = floor((numel(tforms)+1)/2);
    centerImageIdx = idx(centerIdx);

    Tinv = invert(tforms(centerImageIdx));
    for i = 1:numel(tforms)    
        tforms(i).T = tforms(i).T * Tinv.T;
    end

    for i = 1:numel(tforms)           
        [xlim(i,:), ylim(i,:)] = outputLimits(tforms(i), [1 imageSize(i,2)], [1 imageSize(i,1)]);
    end

    maxImageSize = max(imageSize);

    % Find the minimum and maximum output limits. 
    xMin = min([1; xlim(:)]);
    xMax = max([maxImageSize(2); xlim(:)]);

    yMin = min([1; ylim(:)]);
    yMax = max([maxImageSize(1); ylim(:)]);

    % Width and height of panorama.
    width  = round(xMax - xMin);
    height = round(yMax - yMin);

    % Initialize the "empty" panorama.
    panorama = zeros([height width], 'like', I);

    blender = vision.AlphaBlender('Operation', 'Binary mask', ...
        'MaskSource', 'Input port');  

    % Create a 2-D spatial reference object defining the size of the panorama.
    xLimits = [xMin xMax];
    yLimits = [yMin yMax];
    panoramaView = imref2d([height width], xLimits, yLimits);

    %ADD: Panorama Array
    camW = cameraParams.ImageSize(2);
    camH = cameraParams.ImageSize(1);
    partPanoramas = [];
    originTL = [];
    TLtoCent_OS = [];
    TLtoTR_OS = [];
    TLtoBL_OS = [];
    TLtoBR_OS = [];
    
    % Create the panorama.
    for i = 1:numImages

        I = testImgs(:,:,i);

        % Transform I into the panorama.
        warpedImage = imwarp(I, tforms(i), 'OutputView', panoramaView);      

        %Collect origin (Top Left Corner), center, other corners
        tFormsMatrix = tforms(i).T;
        originTL = [originTL;tFormsMatrix(3,1:2)];
        
        TLtoCent_OS = [TLtoCent_OS; [camW/2, camH/2]*tFormsMatrix(1:2,1:2)];
        TLtoTR_OS = [TLtoTR_OS; [camW, 0]*tFormsMatrix(1:2,1:2)];
        TLtoBL_OS = [TLtoBL_OS; [0, camH]*tFormsMatrix(1:2,1:2)];
        TLtoBR_OS = [TLtoBR_OS; [camW, camH]*tFormsMatrix(1:2,1:2)];
        
        
        
        % Generate a binary mask.    
        mask = imwarp(true(size(I,1),size(I,2)), tforms(i), 'OutputView', panoramaView);

        % Overlay the warpedImage onto the panorama.
        panorama = step(blender, panorama, warpedImage, mask);
        partPanoramas = cat(3,partPanoramas,panorama);
    end

    figure('Name','Panorama')
    imshow(panorama)

%PLAY VIDEO
    %https://www.mathworks.com/help/images/ref/videoviewer-app.html
    implay(testImgs,6)
    implay(partPanoramas,6)
    
%putting points on the partial panorama
        %https://www.mathworks.com/matlabcentral/answers/269694-how-to-plot-some-points-on-an-image
%         i = 1;
%         
%         I = testImgs(:,:,i);
%         warpedImage = imwarp(I, tforms(i));
%         figure();
%         imshow(warpedImage);
%         
%         figure();
%         imshow(partPanoramas(:,:,i));
%         hold on;        
%         plot(-xLimits(1),...
%             -yLimits(1),...
%             'b+', 'MarkerSize', 30, 'LineWidth', 2);
%         plot(-xLimits(1)+originTL(i,1),...
%             -yLimits(1)+originTL(i,2),...
%             'r+', 'MarkerSize', 30, 'LineWidth', 2);
%         plot(-xLimits(1)+originTL(i,1)+TLtoCent_OS(i,1),...
%             -yLimits(1)+originTL(i,2)+TLtoCent_OS(i,2),...
%             'g+', 'MarkerSize', 30, 'LineWidth', 2);
%         plot(-xLimits(1)+originTL(i,1)+TLtoTR_OS(i,1),...
%             -yLimits(1)+originTL(i,2)+TLtoTR_OS(i,2),...
%             'r+', 'MarkerSize', 30, 'LineWidth', 2);
%         plot(-xLimits(1)+originTL(i,1)+TLtoBL_OS(i,1),...
%             -yLimits(1)+originTL(i,2)+TLtoBL_OS(i,2),...
%             'r+', 'MarkerSize', 30, 'LineWidth', 2);
%         plot(-xLimits(1)+originTL(i,1)+TLtoBR_OS(i,1),...
%             -yLimits(1)+originTL(i,2)+TLtoBR_OS(i,2),...
%             'r+', 'MarkerSize', 30, 'LineWidth', 2);
    
%arivnd says remove blender for testing
Daytime Drone Data
	The main code used to analyze this set is 'IR_Test_4_0' in the folder 'Drone Camera (Harris)'
	See the ReadMe file in the folder 'Drone Camera (Harris)' for an explanation for the other 'IR_test_' codes

Underwater Vehicle Data
	The main code used to analyze this set using HARRIS POINTS is 'UW_Test_1_Harris' in the folder 'Underwater Camera (Harris)'
	We also have small misc. trials in the folder 'Underwater Camera (Harris)'
		but 'UW_Test_1_Harris' gave the best results
	This data was also analyzed using 'SIFT_allImages_stitch_new11_underwater'
		Found online on MathWorks, slightly modified

Night Drone Data
	We are not uploading the bag file 'uas_kri_night_2020-07-31-20-50-08-001.bag' because it is 17 GB
	We analyzed this data after our presentation on April 21
	It was analyzed using 'IR_Test_4_0_Night' in the folder 'NightImages'
		Same Harris method used for analyzing daytime images

Presentation
	Our presentation is the file 'EECE5554 Group 7 Final Presentation' found here
	It includes our follow-up slides analyzing the Night Data

#!/usr/bin/env python
import rospy
import serial
import numpy as np
from sensor_msgs.msg import Imu
from sensor_msgs.msg import MagneticField

def IMU_Driver(data):   
    imu_msg = Imu()
    MF_msg = MagneticField()
    
    info = str(data)
    endnum = len(info)
    preParse = info[1:endnum-8]
    parse = preParse.split(',')
    
    rospy.loginfo(preParse)
    
    try:
    	yaw = float(parse[1])*np.pi/180	#degrees to radians
    	pitch = float(parse[2])*np.pi/180	#degrees to radians
    	roll = float(parse[3])*np.pi/180	#degrees to radians
    	magX = float(parse[4])/10000		#gauss to teslas
    	magY = float(parse[5])/10000		#gauss to teslas
    	magZ = float(parse[6])/10000		#gauss to teslas
    	accX = float(parse[7])			#already in m/s^2
    	accY = float(parse[8])			#already in m/s^2
    	accZ = float(parse[9])			#already in m/s^2
    	gyroX = float(parse[10])		#already in rad/s
    	gyroY = float(parse[11])		#already in rad/s
    	gyroZ = float(parse[12])		#already in rad/s
    except ValueError:
    	yaw = 0.0
    	pitch = 0.0
    	roll = 0.0
    	magX = 0.0
    	magY = 0.0
    	magZ = 0.0
    	accX = 0.0
    	accY = 0.0
    	accZ = 0.0
    	gyroX = 0.0    
    	gyroY = 0.0
    	gyroZ = 0.0
    
    #Quaternions 
    #https://automaticaddison.com/how-to-convert-euler-angles-to-quaternions-using-python/
    qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
    qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
    qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
    
    #Define IMU Message
    #http://docs.ros.org/en/melodic/api/sensor_msgs/html/msg/Imu.html
    imu_msg.header.seq = seq
    imu_msg.header.stamp = rospy.Time.now()
    imu_msg.header.frame_id = 'IMU'
    imu_msg.orientation.x=qx
    imu_msg.orientation.y=qy
    imu_msg.orientation.z=qz
    imu_msg.orientation.w=qw
    imu_msg.angular_velocity.x=gyroX
    imu_msg.angular_velocity.y=gyroY
    imu_msg.angular_velocity.z=gyroZ
    imu_msg.linear_acceleration.x=accX
    imu_msg.linear_acceleration.y=accY
    imu_msg.linear_acceleration.z=accZ
    
    #Define Magnetic Field Message 
    #http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/MagneticField.html
    MF_msg.header.seq = seq
    MF_msg.header.stamp = rospy.Time.now()
    MF_msg.header.frame_id = 'Magnetic Field'
    MF_msg.magnetic_field.x = magX
    MF_msg.magnetic_field.y = magY
    MF_msg.magnetic_field.z = magZ
    
    #Publish messages
    rospy.loginfo(imu_msg)
    IMUpub.publish(imu_msg)
    rospy.loginfo(MF_msg)
    MFpub.publish(MF_msg)
    rate.sleep()

if __name__ == '__main__':
    SENSOR_NAME = "IMU"
    serial_port = rospy.get_param('~port','/dev/ttyUSB1')
    serial_baud = rospy.get_param('~baudrate',115200)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
           
    line = port.readline() # gets the data from the sensor
    
    IMUpub = rospy.Publisher('IMU', Imu, queue_size=10)
    MFpub = rospy.Publisher('MF', MagneticField, queue_size=10)
    rospy.init_node('IMU_Node', anonymous=True)
    rate = rospy.Rate(40) # 40hz
    
    seq = 0;
    
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            reading = str(line)
            seq = seq + 1
            
            #test = '$VNYMR,+013.623,-011.048,-008.452,+00.3202,-00.1229,+00.4452,-01.824,+01.363,-09.242,-00.0010401'
            #test2 = b'$VNYMR,+013.522,-011.045,-008.431,+00.3165,-00.1178,+00.4407,-01.801,+01.346,-09.234,+00.000870,+00.000527,+00.000229*6B\r\n'
            #IMU_Driver(test2)
            
            if reading.startswith("b'$VNYMR"):
            	 IMU_Driver(line)
            
    except rospy.ROSInterruptException:
        port.close()
    
    except serial.serialutil.SerialException:
        rospy.loginfo("Shutting down IMU node...")

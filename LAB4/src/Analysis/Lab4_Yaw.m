%EECE 5554 Lab 4 Navigation, Yaw Estimate

clear all;
close all;

%GETTING BAG DATA FROM CIRCLE DRIVE
    %open bags
    bagPath = "bags/Roundabout.bag";
    bagSelect = rosbag(bagPath);
    bagInfo = rosbag('info',bagPath);
    IMUtopic = select(bagSelect,'Topic','IMU');
    MFtopic = select(bagSelect,'Topic','MF');
    GPStopic = select(bagSelect,'Topic','GPS');
    IMUread = readMessages(IMUtopic,'DataFormat','struct');
    MFread = readMessages(MFtopic,'DataFormat','struct');
    GPSread = readMessages(GPStopic,'DataFormat','struct');

    %extract GPS data
    UTMEast = cellfun(@(m) double(m.UtmEasting),GPSread);
    UTMEastMean = mean(UTMEast);
    UTMEast = UTMEast-UTMEastMean; %Center data around mean
    UTMNorth = cellfun(@(m) double(m.UtmNorthing),GPSread);
    UTMNorthMean = mean(UTMNorth);
    UTMNorth = UTMNorth-UTMNorthMean; %Center data around mean
    Alt = cellfun(@(m) double(m.Altitude),GPSread);
    q = cellfun(@(m) double(m.Quality),GPSread);

    %Fix Time
    GPStimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,GPSread);
    GPStimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,GPSread));
    GPStimeNS = GPStimeNS/1000000000;
    GPS_t = double(GPStimeS) + GPStimeNS;
    GPS_t = GPS_t-GPS_t(1);

    %extract IMU data
    qX = cellfun(@(m) struct(m.Orientation).X,IMUread);
    qY = cellfun(@(m) struct(m.Orientation).Y,IMUread);
    qZ = cellfun(@(m) struct(m.Orientation).Z,IMUread);
    qW = cellfun(@(m) struct(m.Orientation).W,IMUread);
    gyroX = cellfun(@(m) struct(m.AngularVelocity).X,IMUread);
    gyroY = cellfun(@(m) struct(m.AngularVelocity).Y,IMUread);
    gyroZ = cellfun(@(m) struct(m.AngularVelocity).Z,IMUread);
    accX = cellfun(@(m) struct(m.LinearAcceleration).X,IMUread);
    accY = cellfun(@(m) struct(m.LinearAcceleration).Y,IMUread);
    accZ = cellfun(@(m) struct(m.LinearAcceleration).Z,IMUread);

    %quaternion to euler
    %https://www.mathworks.com/help/robotics/ref/quat2eul.html
    eul = quat2eul([qW qX qY qZ]); %input is wxyz, output is zyx
    roll_from_IMU = eul(:,3);
    pitch_from_IMU = eul(:,2);
    yaw_from_IMU = eul(:,1);

    %Fix Time
    IMUtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,IMUread);
    IMUtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,IMUread));
    IMUtimeNS = IMUtimeNS/1000000000;
    IMU_t = double(IMUtimeS) + IMUtimeNS;
    IMU_t = IMU_t-IMU_t(1);

    %extract MF data
    magX = cellfun(@(m) struct(m.MagneticField_).X,MFread);
    magY = cellfun(@(m) struct(m.MagneticField_).Y,MFread);
    magZ = cellfun(@(m) struct(m.MagneticField_).Z,MFread);

    %fix time
    MFtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,MFread);
    MFtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,MFread));
    MFtimeNS = MFtimeNS/1000000000;
    MF_t = double(MFtimeS) + MFtimeNS;
    MF_t = MF_t - MF_t(1);

% %PLOTTING CIRCLE DATA FOR VISUALIZATION
%     %UTM Graph for Visualization
%     figure(1)
%     plot3(UTMEast,UTMNorth,GPS_t,'-o');
%     hold on
%     xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');zlabel('Time (s)');
%     title('Easting vs Northing vs Time');
%     grid on;
%     hold off
% 
%     %Euler Graph for visualization
%     figure(2)
%     hold on
%     scatter(IMU_t,roll_from_IMU,'r.');
%     scatter(IMU_t,pitch_from_IMU,'b.');
%     scatter(IMU_t,yaw_from_IMU,'g.');
%     xlabel('Time (s)'); ylabel('Orientation (rad)');
%     legend({'X','Y','Z'},'Location','east');
%     title("Orientation vs Time");
%     hold off
% 
%     %Gyro Graph for visualization
%     figure(3)
%     hold on
%     scatter(IMU_t,gyroX,'r.');
%     scatter(IMU_t,gyroY,'b.');
%     scatter(IMU_t,gyroZ,'g.');
%     xlabel('Time (s)'); ylabel('Angular Velocity (rad/s)');
%     legend({'GyroX','GyroY','GyroZ'},'Location','east');
%     title("Angular Velocity vs Time");
%     hold off
% 
%     %Time vs Mag Flux in the axes
%     figure(4)
%     hold on
%     scatter(MF_t,magX,'r.');
%     scatter(MF_t,magY,'b.');
%     scatter(MF_t,magZ,'g.');
%     xlabel('Time (s)'); ylabel('Magnetic Fluctuation (Teslas)');
%     legend({'magX','magY','magZ'},'Location','east');
%     title("Mag Flux vs Time");
%     hold off

%MAGNETOMETER HARD-IRON AND SOFT-IRON DATA CORRECTION
    %Fixing data range
    Fs = 40; %Hz
    tC = 70*Fs;%cuts idle time and first (bad) loop
    tCF = length(MF_t); %length(MF_t) is slightly shorter than length(IMU_t)
    gyroX = gyroX(tC:tCF);
    gyroY = gyroY(tC:tCF);
    gyroZ = gyroZ(tC:tCF);
    yaw_from_IMU = yaw_from_IMU(tC:tCF);
    IMU_t = IMU_t(tC:tCF);
    magX = magX(tC:tCF);
    magY = magY(tC:tCF);
    MF_t = MF_t(tC:tCF);

    %HI and SI data correction
    %https://www.vectornav.com/resources/inertial-navigation-primer/specifications--and--error-budgets/specs-hsicalibration
    bHIX = mean(magX);
    bHIY = mean(magY);
    magHI = [transpose(magX)-bHIX; transpose(magY)-bHIY];
    bSI =   eye(2); %no soft iron effect observed, so this can stay as I
    magCor = bSI*magHI;
    magCor = transpose(magCor);

%     %Mag Flux X vs Mag Flux Y
%     figure(5)
%     %plot3(magX,magY,MF_t,'-.');
%     %plot3(magX,magY,magZ,'-.');
%     hold on
%     scatter(magX,magY,'r.');
%     scatter(magCor(:,1),magCor(:,2),'b.');
%     xlabel('Magnetic Fluctuation X-Axis (Teslas)');
%     ylabel('Magnetic Fluctuation Y-Axis (Teslas)');
%     legend({'Mag','MagCor'},'Location','east');
%     title("Mag Flux X-Axis vs Y-Axis (Corrected)");
%     grid on
%     axis equal
%     hold off

%GETTING BAG DATA FROM MAIN DRIVE
    %open bags
    bagPath = "bags/Driving.bag";
    bagSelect = rosbag(bagPath);
    bagInfo = rosbag('info',bagPath);
    IMUtopic = select(bagSelect,'Topic','IMU');
    MFtopic = select(bagSelect,'Topic','MF');
    GPStopic = select(bagSelect,'Topic','GPS');
    IMUread = readMessages(IMUtopic,'DataFormat','struct');
    MFread = readMessages(MFtopic,'DataFormat','struct');
    GPSread = readMessages(GPStopic,'DataFormat','struct');

    %extract GPS data
    UTMEast = cellfun(@(m) double(m.UtmEasting),GPSread);    
    UTMNorth = cellfun(@(m) double(m.UtmNorthing),GPSread);
    Alt = cellfun(@(m) double(m.Altitude),GPSread);
    q = cellfun(@(m) double(m.Quality),GPSread);

    %Fix Time
    GPStimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,GPSread);
    GPStimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,GPSread));
    GPStimeNS = GPStimeNS/1000000000;
    GPS_t = double(GPStimeS) + GPStimeNS;
    GPS_t = GPS_t-GPS_t(1);

    %Removing GPS Outliers (code gives 0 lat/long if error), fixing mean
    UTMEastMean = mean(UTMEast);
    UTMEastStdDev = std(UTMEast);
    badNums = [];
    for i=1:length(UTMEast)
        if abs(UTMEastMean-UTMEast(i)) > abs(3*UTMEastStdDev)
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        UTMEast(i) = [];
        UTMNorth(i) = [];
        Alt(i) = [];
        q(i) = [];
        GPS_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
    UTMEastMean = mean(UTMEast);
    UTMEast = UTMEast-UTMEastMean; %Center data around mean
    UTMNorthMean = mean(UTMNorth);
    UTMNorth = UTMNorth-UTMNorthMean; %Center data around mean
    
    %extract IMU data
    qX = cellfun(@(m) struct(m.Orientation).X,IMUread);
    qY = cellfun(@(m) struct(m.Orientation).Y,IMUread);
    qZ = cellfun(@(m) struct(m.Orientation).Z,IMUread);
    qW = cellfun(@(m) struct(m.Orientation).W,IMUread);
    gyroX = cellfun(@(m) struct(m.AngularVelocity).X,IMUread);
    gyroY = cellfun(@(m) struct(m.AngularVelocity).Y,IMUread);
    gyroZ = cellfun(@(m) struct(m.AngularVelocity).Z,IMUread);
    accX = cellfun(@(m) struct(m.LinearAcceleration).X,IMUread);
    accY = cellfun(@(m) struct(m.LinearAcceleration).Y,IMUread);
    accZ = cellfun(@(m) struct(m.LinearAcceleration).Z,IMUread);

    %Fix Time
    IMUtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,IMUread);
    IMUtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,IMUread));
    IMUtimeNS = IMUtimeNS/1000000000;
    IMU_t = double(IMUtimeS) + IMUtimeNS;
    IMU_t = IMU_t-IMU_t(1);

    %removing IMU outliers (code gives 0 if error)
    badNums = [];
    for i=1:length(gyroX)
        if gyroX(i) == 0 && gyroY(i) == 0 && gyroZ(i) == 0
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        qX(i) = [];
        qY(i) = [];
        qZ(i) = [];
        qW(i) = [];        
        gyroX(i) = [];
        gyroY(i) = [];
        gyroZ(i) = [];
        accX(i) = [];
        accY(i) = [];
        accZ(i) = [];
        IMU_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
    %quaternion to euler
    %https://www.mathworks.com/help/robotics/ref/quat2eul.html
    eul = quat2eul([qW qX qY qZ]); %input is wxyz, output is zyx
    roll_from_IMU = eul(:,3);
    pitch_from_IMU = eul(:,2);
    yaw_from_IMU = eul(:,1);
    
    %extract MF data
    magX = cellfun(@(m) struct(m.MagneticField_).X,MFread);
    magY = cellfun(@(m) struct(m.MagneticField_).Y,MFread);
    magZ = cellfun(@(m) struct(m.MagneticField_).Z,MFread);

    %fix time
    MFtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,MFread);
    MFtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,MFread));
    MFtimeNS = MFtimeNS/1000000000;
    MF_t = double(MFtimeS) + MFtimeNS;
    MF_t = MF_t - MF_t(1);
    
    %removing MF outliers (code gives 0 if error)
    badNums = [];
    for i=1:length(magX)
        if magX(i) == 0 && magY(i) == 0 && magZ(i) == 0
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        magX(i) = [];
        magY(i) = [];
        magZ(i) = [];
        MF_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
% %PLOTTING MAIN GRAPH DATA FOR VISUALIZATION
%     %UTM Graph for Visualization
%     figure(6)
%     %plot3(UTMEast,UTMNorth,GPS_t,'-o');
%     scatter(UTMEast,UTMNorth,'r.');
%     hold on
%     xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');zlabel('Time (s)');
%     title('Easting vs Northing');
%     grid on;
%     hold off
% 
%     %Euler Graph for visualization
%     figure(7)
%     hold on
%     scatter(IMU_t,roll_from_IMU,'r.');
%     scatter(IMU_t,pitch_from_IMU,'b.');
%     scatter(IMU_t,yaw_from_IMU,'g.');
%     xlabel('Time (s)'); ylabel('Orientation (rad)');
%     legend({'X','Y','Z'},'Location','southeast');
%     title("Orientation vs Time");
%     hold off
% 
%     %Gyro Graph for visualization
%     figure(8)
%     hold on
%     scatter(IMU_t,gyroX,'r.');
%     scatter(IMU_t,gyroY,'b.');
%     scatter(IMU_t,gyroZ,'g.');
%     xlabel('Time (s)'); ylabel('Angular Velocity (rad/s)');
%     legend({'GyroX','GyroY','GyroZ'},'Location','east');
%     title("Angular Velocity vs Time");
%     hold off
% 
%     %Time vs Mag Flux in the axes
%     figure(9)
%     hold on
%     scatter(MF_t,magX,'r.');
%     scatter(MF_t,magY,'b.');
%     scatter(MF_t,magZ,'g.');
%     xlabel('Time (s)'); ylabel('Magnetic Fluctuation (Teslas)');
%     legend({'magX','magY','magZ'},'Location','east');
%     title("Mag Flux vs Time");
%     hold off

%IRON EFFECTS CORRECTION FOR MAIN DRIVE, USING CIRCLE DATA
%1. Submit plots showing the magnetometer data before and after the correction in your report.
    %Fixing data range
    Fs = 40; %Hz
    tC = 1;
    tCF = length(MF_t); %length(MF_t) is slightly shorter than length(IMU_t)
    gyroX = gyroX(tC:tCF);
    gyroY = gyroY(tC:tCF);
    gyroZ = gyroZ(tC:tCF);
    yaw_from_IMU = yaw_from_IMU(tC:tCF);
    IMU_t = IMU_t(tC:tCF);
    magX = magX(tC:tCF);
    magY = magY(tC:tCF);
    MF_t = MF_t(tC:tCF);
    
    %HI and SI data correction
    magHI = [transpose(magX)-bHIX; transpose(magY)-bHIY];
    bSI =   eye(2); %no soft iron effect observed, so this can stay as I
    magCor = bSI*magHI;
    magCor = transpose(magCor);

%     %Mag Flux X vs Mag Flux Y
%     figure(10)
%     hold on
%     scatter(magX,magY,'r.');
%     scatter(magCor(:,1),magCor(:,2),'b.');
%     xlabel('Magnetic Fluctuation X-Axis (Teslas)');
%     ylabel('Magnetic Fluctuation Y-Axis (Teslas)');
%     legend({'Mag','MagCor'},'Location','east');
%     title("Mag Flux X-Axis vs Y-Axis (Corrected)");
%     grid on
%     axis equal
%     hold off

%CALCULATE YAW
%2. Compare the yaw angle from above two methods.(Magnetometer vs. Yaw Integrated from Gyro).
    %https://students.iitk.ac.in/roboclub/2017/12/21/Beginners-Guide-to-IMU.html
    %assume flat, so no need to redefine magX and magY
    yaw_from_mag = atan2(-magCor(:,2),magCor(:,1));
    
    yaw_from_gyro = cumtrapz(IMU_t,gyroZ);
    
    %Comparison of Yaws
    %mag Yaw is noisy, so I will smooth it
    window = 200;
    yaw_from_mag_smooth = smoothdata(yaw_from_mag,'movmean',window);
    figure(12)
    hold on
    scatter(IMU_t,yaw_from_gyro,'g.');
    scatter(IMU_t,yaw_from_IMU,'r.');
    scatter(IMU_t,yaw_from_mag_smooth,'b.');    
    xlabel('Time (s)'); ylabel('Orientation (rad)');
    legend({'Gyro Yaw','IMU Yaw','Mag Yaw'},'Location','southeast');
    title("Yaw vs Time");
    hold off
     
%FILTERING YAW DATA
%3. Compare your result to the yaw angle computed by the IMU and write down your observations.
    %https://www.mathworks.com/help/signal/ref/highpass.html
    FHpass = .15; %passband frequency between 0 and Fs/2
    FLpass = .1;
    figure(13);
    highpass(yaw_from_gyro,FHpass,Fs);
    figure(14);
    lowpass(yaw_from_mag_smooth,FLpass,Fs);
    yaw_from_gyro_HP = highpass(yaw_from_gyro,FHpass,Fs);
    yaw_from_mag_LP = lowpass(yaw_from_mag_smooth,FLpass,Fs);
    yaw_comp_filter = yaw_from_gyro_HP + yaw_from_mag_LP;
    yaw_comp_filter = wrapToPi(yaw_comp_filter);

    %Comparison of Yaws
    figure(15)
    hold on
    scatter(IMU_t,yaw_from_gyro_HP,'g.');
    scatter(IMU_t,yaw_from_mag_LP,'m.');
    scatter(IMU_t,yaw_comp_filter,'b.');
    scatter(IMU_t,yaw_from_IMU,'r.');    
    xlabel('Time (s)'); ylabel('Orientation (rad)');
    legend({'Gyro Yaw HP','Mag Yaw LP','Comp Filter Yaw','IMU Yaw'},'Location','east');
    title("Yaw vs Time for FHpass "+FHpass+", FLpass "+FLpass);
    hold off

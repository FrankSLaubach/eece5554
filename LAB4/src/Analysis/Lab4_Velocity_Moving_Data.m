%EECE 5554 Lab 4 Navigation, Forward Velocity Estimate

clear all
close all

%GETTING BAG DATA FROM MAIN DRIVE
    %open bags
    bagPath = "bags/Driving.bag";
    bagSelect = rosbag(bagPath);
    bagInfo = rosbag('info',bagPath);
    IMUtopic = select(bagSelect,'Topic','IMU');
    MFtopic = select(bagSelect,'Topic','MF');
    GPStopic = select(bagSelect,'Topic','GPS');
    IMUread = readMessages(IMUtopic,'DataFormat','struct');
    MFread = readMessages(MFtopic,'DataFormat','struct');
    GPSread = readMessages(GPStopic,'DataFormat','struct');

    %extract GPS data
    UTMEast = cellfun(@(m) double(m.UtmEasting),GPSread);    
    UTMNorth = cellfun(@(m) double(m.UtmNorthing),GPSread);
    Alt = cellfun(@(m) double(m.Altitude),GPSread);
    q = cellfun(@(m) double(m.Quality),GPSread);

    %Fix Time
    GPStimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,GPSread);
    GPStimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,GPSread));
    GPStimeNS = GPStimeNS/1000000000;
    GPS_t = double(GPStimeS) + GPStimeNS;
    GPS_t = GPS_t-GPS_t(1);

    %Removing GPS Outliers (code gives 0 lat/long if error), fixing mean
    UTMEastMean = mean(UTMEast);
    UTMEastStdDev = std(UTMEast);
    badNums = [];
    for i=1:length(UTMEast)
        if abs(UTMEastMean-UTMEast(i)) > abs(3*UTMEastStdDev)
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        UTMEast(i) = [];
        UTMNorth(i) = [];
        Alt(i) = [];
        q(i) = [];
        GPS_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
    UTMEastMean = mean(UTMEast);
    UTMEast = UTMEast-UTMEastMean; %Center data around mean
    UTMNorthMean = mean(UTMNorth);
    UTMNorth = UTMNorth-UTMNorthMean; %Center data around mean
    
    %extract IMU data
    qX = cellfun(@(m) struct(m.Orientation).X,IMUread);
    qY = cellfun(@(m) struct(m.Orientation).Y,IMUread);
    qZ = cellfun(@(m) struct(m.Orientation).Z,IMUread);
    qW = cellfun(@(m) struct(m.Orientation).W,IMUread);
    gyroX = cellfun(@(m) struct(m.AngularVelocity).X,IMUread);
    gyroY = cellfun(@(m) struct(m.AngularVelocity).Y,IMUread);
    gyroZ = cellfun(@(m) struct(m.AngularVelocity).Z,IMUread);
    accX = cellfun(@(m) struct(m.LinearAcceleration).X,IMUread);
    accY = cellfun(@(m) struct(m.LinearAcceleration).Y,IMUread);
    accZ = cellfun(@(m) struct(m.LinearAcceleration).Z,IMUread);

    %Fix Time
    IMUtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,IMUread);
    IMUtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,IMUread));
    IMUtimeNS = IMUtimeNS/1000000000;
    IMU_t = double(IMUtimeS) + IMUtimeNS;
    IMU_t = IMU_t-IMU_t(1);

    %removing IMU outliers (code gives 0 if error)
    badNums = [];
    for i=1:length(gyroX)
        if gyroX(i) == 0 && gyroY(i) == 0 && gyroZ(i) == 0
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        qX(i) = [];
        qY(i) = [];
        qZ(i) = [];
        qW(i) = [];        
        gyroX(i) = [];
        gyroY(i) = [];
        gyroZ(i) = [];
        accX(i) = [];
        accY(i) = [];
        accZ(i) = [];
        IMU_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
    %quaternion to euler
    %https://www.mathworks.com/help/robotics/ref/quat2eul.html
    eul = quat2eul([qW qX qY qZ]); %input is wxyz, output is zyx
    roll_from_IMU = eul(:,3);
    pitch_from_IMU = eul(:,2);
    yaw_from_IMU = eul(:,1);
    
    %extract MF data
    magX = cellfun(@(m) struct(m.MagneticField_).X,MFread);
    magY = cellfun(@(m) struct(m.MagneticField_).Y,MFread);
    magZ = cellfun(@(m) struct(m.MagneticField_).Z,MFread);

    %fix time
    MFtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,MFread);
    MFtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,MFread));
    MFtimeNS = MFtimeNS/1000000000;
    MF_t = double(MFtimeS) + MFtimeNS;
    MF_t = MF_t - MF_t(1);
    
    %removing MF outliers (code gives 0 if error)
    badNums = [];
    for i=1:length(magX)
        if magX(i) == 0 && magY(i) == 0 && magZ(i) == 0
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        magX(i) = [];
        magY(i) = [];
        magZ(i) = [];
        MF_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
%PLOTTING MAIN GRAPH DATA FOR VISUALIZATION
    %UTM Graph for Visualization
    figure(1)
    scatter(UTMEast,UTMNorth,'r.');
    hold on
    xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');zlabel('Time (s)');
    title('Easting vs Northing');
    grid on;
    hold off

    %Accelerometer Graph for visualization
    figure(2)
    hold on
    scatter(IMU_t,accY,'b.');
    scatter(IMU_t,accX,'r.');
    scatter(IMU_t,accZ,'g.');
    xlabel('Time (s)'); ylabel('Linear Acceleration (m/s^2)');
    legend({'accY','accX','accZ'},'Location','east');
    title("Linear Acceleration vs Time");
    hold off

%CALCULATING FORWARD VELOCITY
%1. Plot both the velocity estimates.  
    %velocity from GPS
    %https://www.mathworks.com/matlabcentral/answers/385326-calculate-velocity-from-position-and-time
    EastVel = [];
    for i=2:(length(UTMEast))
        deltaX = UTMEast(i)-UTMEast(i-1);
        deltaT = GPS_t(i)-GPS_t(i-1);%these should all be ~1 s
        instVel = deltaX/deltaT;
        EastVel=[EastVel;instVel];
    end
    
    NorthVel = [];
    for i=2:(length(UTMNorth))
        deltaY = UTMNorth(i)-UTMNorth(i-1);
        deltaT = GPS_t(i)-GPS_t(i-1);%these should all be ~1 s
        instVel = deltaY/deltaT;
        NorthVel =[NorthVel;instVel];
    end
    
    vel_from_GPS = sqrt(EastVel.^2+NorthVel.^2);
    
    %velocity from linear acceleration
    velX_from_accX = cumtrapz(IMU_t,accX);
    
    %Best Fit Line %https://www.mathworks.com/matlabcentral/answers/377139-how-to-plot-best-fit-line
    x = IMU_t;
    y = velX_from_accX;
    coefficients = polyfit(x, y, 2);
    xFit = linspace(min(x), max(x), 32064);
    yFit = polyval(coefficients , xFit);
    yDiff = y-yFit.';

    %Comparison of Vels
    figure(3)
    hold on    
    scatter(GPS_t(2:length(GPS_t)),vel_from_GPS,'b.');
    scatter(IMU_t,velX_from_accX,'r.');
    plot(xFit, yFit, '--', 'LineWidth', 1); % Plot fitted line.
    xlabel('Time (s)'); ylabel('Velocity (m/s)');
    legend({'Vel from GPS','Vel from IMU','IMU Vel Line of Best Fit'},'Location','east');
    title("Velocity vs Time");
    hold off
    
    %Correction Attempt 1
    figure(4)
    hold on    
    scatter(GPS_t(2:length(GPS_t)),vel_from_GPS,'b.');
    scatter(IMU_t,yDiff,'r.');
    xlabel('Time (s)'); ylabel('Velocity (m/s)');
    legend({'Vel from GPS','Corrected Vel from IMU'},'Location','east');
    title("Velocity vs Time");
    hold off
    
%ADJUSTING ACCELERATION DATA ATTEMPT 2
    %smooth accX
    window = 200;
    accX_smooth = smoothdata(accX,'movmean',window);
    figure(5)
    hold on
    scatter(IMU_t,accX,'r.');
    scatter(IMU_t,accX_smooth,'b.');
    xlabel('Time (s)'); ylabel('Linear Acceleration (m/s^2)');
    legend({'accX','accX Smooth'},'Location','east');
    title("Linear Acceleration vs Time");
    hold off
    
    %velocity from linear acceleration
    velX_from_smooth_accX = cumtrapz(IMU_t,accX_smooth);
    
    %Best Fit Line %https://www.mathworks.com/matlabcentral/answers/377139-how-to-plot-best-fit-line
    x = IMU_t;
    y = velX_from_smooth_accX;
    coefficients = polyfit(x, y, 2);
    xFit = linspace(min(x), max(x), 32064);
    yFit = polyval(coefficients , xFit);
    yDiff = y-yFit.';
    
    %Comparison of Vels
    figure(6)
    hold on    
    scatter(GPS_t(2:length(GPS_t)),vel_from_GPS,'b.');
    scatter(IMU_t,yDiff,'r.');
    xlabel('Time (s)'); ylabel('Velocity (m/s)');
    legend({'Vel from GPS','Corrected Vel from IMU'},'Location','east');
    title("Velocity vs Time");
    hold off
    
    
%ADJUSTING ACCELERATION DATA ATTEMPT 2.5
    %Adjust to Mean
    accXMean = mean(accX);
    accX2=accX-accXMean;

    %velocity from linear acceleration
    velX_from_accX2 = cumtrapz(IMU_t,accX2);
    
    %Comparison of Vels
    figure(7)
    hold on    
    scatter(GPS_t(2:length(GPS_t)),vel_from_GPS,'b.');
    scatter(IMU_t,velX_from_accX2,'r.');
    xlabel('Time (s)'); ylabel('Velocity (m/s)');
    legend({'Vel from GPS','Corrected Vel from IMU'},'Location','east');
    title("Velocity vs Time");
    hold off

%ADJUSTING ACCELERATION DATA ATTEMPT 3
    accX3 = accX_smooth;
    frameLength = 40;
    
    movFrame = [];    
    for i=1:frameLength
        movFrame = [movFrame;accX3(i)];
    end
    
    displacement = mean(movFrame); %start at acc = 0
    for i=1:frameLength
        accX3(i) = accX3(i)-displacement;
    end
    
    for i=frameLength+1:length(accX3)
        accX3(i) = accX3(i) - displacement;
        
        instMean = mean(movFrame);
        instStd = std(movFrame);
        
        if instStd < 0.001
            %disp("from "+(i-frameLength)+" to "+i+": "+instMean);
            displacement = displacement + instMean;
            accX3(i-frameLength:i-1) = zeros([frameLength 1]);
        end        
        
        movFrame(1)=[];
        movFrame = [movFrame;accX3(i)];
    end
    
    %velocity from linear acceleration
    velX_from_accX3 = cumtrapz(IMU_t,accX3);
    
    figure(8)
    hold on
    scatter(IMU_t,accX,'r.');
    scatter(IMU_t,accX3,'b.');
    xlabel('Time (s)'); ylabel('Linear Acceleration (m/s^2)');
    legend({'accX','accX flatt'},'Location','east');
    title("Linear Acceleration vs Time");
    hold off
    
    figure(9)
    hold on    
    scatter(GPS_t(2:length(GPS_t)),vel_from_GPS,'b.');
    scatter(IMU_t,velX_from_accX3,'r.');
    xlabel('Time (s)'); ylabel('Velocity (m/s)');
    legend({'Vel from GPS','Corrected Vel from IMU'},'Location','east');
    title("Velocity vs Time ("+frameLength+" datapoint window)");
    hold off
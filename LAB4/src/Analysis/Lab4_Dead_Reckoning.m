%EECE 5554 Lab 4 Navigation, Dead Reckoning

clear all
close all

%GETTING BAG DATA FROM MAIN DRIVE
    %open bags
    bagPath = "bags/Driving.bag";
    bagSelect = rosbag(bagPath);
    bagInfo = rosbag('info',bagPath);
    IMUtopic = select(bagSelect,'Topic','IMU');
    MFtopic = select(bagSelect,'Topic','MF');
    GPStopic = select(bagSelect,'Topic','GPS');
    IMUread = readMessages(IMUtopic,'DataFormat','struct');
    MFread = readMessages(MFtopic,'DataFormat','struct');
    GPSread = readMessages(GPStopic,'DataFormat','struct');

    %extract GPS data
    UTMEast = cellfun(@(m) double(m.UtmEasting),GPSread);    
    UTMNorth = cellfun(@(m) double(m.UtmNorthing),GPSread);
    Alt = cellfun(@(m) double(m.Altitude),GPSread);
    q = cellfun(@(m) double(m.Quality),GPSread);

    %Fix Time
    GPStimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,GPSread);
    GPStimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,GPSread));
    GPStimeNS = GPStimeNS/1000000000;
    GPS_t = double(GPStimeS) + GPStimeNS;
    GPS_t = GPS_t-GPS_t(1);

    %Removing GPS Outliers (code gives 0 lat/long if error), fixing mean
    UTMEastMean = mean(UTMEast);
    UTMEastStdDev = std(UTMEast);
    badNums = [];
    for i=1:length(UTMEast)
        if abs(UTMEastMean-UTMEast(i)) > abs(3*UTMEastStdDev)
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        UTMEast(i) = [];
        UTMNorth(i) = [];
        Alt(i) = [];
        q(i) = [];
        GPS_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
    UTMEast = UTMEast-UTMEast(1); %Start at 0
    UTMNorth = UTMNorth-UTMNorth(1); %Start at 0
    
    %extract IMU data
    qX = cellfun(@(m) struct(m.Orientation).X,IMUread);
    qY = cellfun(@(m) struct(m.Orientation).Y,IMUread);
    qZ = cellfun(@(m) struct(m.Orientation).Z,IMUread);
    qW = cellfun(@(m) struct(m.Orientation).W,IMUread);
    gyroX = cellfun(@(m) struct(m.AngularVelocity).X,IMUread);
    gyroY = cellfun(@(m) struct(m.AngularVelocity).Y,IMUread);
    gyroZ = cellfun(@(m) struct(m.AngularVelocity).Z,IMUread);
    accX = cellfun(@(m) struct(m.LinearAcceleration).X,IMUread);
    accY = cellfun(@(m) struct(m.LinearAcceleration).Y,IMUread);
    accZ = cellfun(@(m) struct(m.LinearAcceleration).Z,IMUread);

    %Fix Time
    IMUtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,IMUread);
    IMUtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,IMUread));
    IMUtimeNS = IMUtimeNS/1000000000;
    IMU_t = double(IMUtimeS) + IMUtimeNS;
    IMU_t = IMU_t-IMU_t(1);

    %removing IMU outliers (code gives 0 if error)
    badNums = [];
    for i=1:length(gyroX)
        if gyroX(i) == 0 && gyroY(i) == 0 && gyroZ(i) == 0
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        qX(i) = [];
        qY(i) = [];
        qZ(i) = [];
        qW(i) = [];        
        gyroX(i) = [];
        gyroY(i) = [];
        gyroZ(i) = [];
        accX(i) = [];
        accY(i) = [];
        accZ(i) = [];
        IMU_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
    %quaternion to euler
    %https://www.mathworks.com/help/robotics/ref/quat2eul.html
    eul = quat2eul([qW qX qY qZ]); %input is wxyz, output is zyx
    roll_from_IMU = eul(:,3);
    pitch_from_IMU = eul(:,2);
    yaw_from_IMU = eul(:,1);
    
    %extract MF data
    magX = cellfun(@(m) struct(m.MagneticField_).X,MFread);
    magY = cellfun(@(m) struct(m.MagneticField_).Y,MFread);
    magZ = cellfun(@(m) struct(m.MagneticField_).Z,MFread);

    %fix time
    MFtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,MFread);
    MFtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,MFread));
    MFtimeNS = MFtimeNS/1000000000;
    MF_t = double(MFtimeS) + MFtimeNS;
    MF_t = MF_t - MF_t(1);
    
    %removing MF outliers (code gives 0 if error)
    badNums = [];
    for i=1:length(magX)
        if magX(i) == 0 && magY(i) == 0 && magZ(i) == 0
            badNums = [badNums, i];
        end
    end
    
    bnCounter = 0;
    for i=badNums
        i = i-bnCounter;
        magX(i) = [];
        magY(i) = [];
        magZ(i) = [];
        MF_t(i) = [];
        bnCounter=bnCounter+1;
    end
    
% %PLOTTING MAIN GRAPH DATA FOR VISUALIZATION
%     figure(1)
%     hold on
%     scatter(IMU_t,accX,'r.');
%     scatter(IMU_t,accY,'b.');
%     scatter(IMU_t,accZ,'g.');
%     xlabel('Time (s)'); ylabel('Linear Acceleration (m/s^2)');
%     legend({'accX','accY','accZ'},'Location','east');
%     title("Linear Acceleration vs Time");
%     hold off
%     
%     figure(2)
%     hold on
%     scatter(IMU_t,gyroX,'r.');
%     scatter(IMU_t,gyroY,'b.');
%     scatter(IMU_t,gyroZ,'g.');
%     xlabel('Time (s)'); ylabel('Angular Velocity (rad/s)');
%     legend({'GyroX','GyroY','GyroZ'},'Location','east');
%     title("Angular Velocity vs Time");
%     hold off

%MAGNETOMETER CALIBRATION & YAW CALCULATION (FROM PART 1)
    %Fixing data range
    Fs = 40; %Hz
    tC = 1;
    tCF = length(MF_t); %length(MF_t) is slightly shorter than length(IMU_t)
    accX = accX(tC:tCF);
    accY = accY(tC:tCF);
    accZ = accZ(tC:tCF);
    gyroX = gyroX(tC:tCF);
    gyroY = gyroY(tC:tCF);
    gyroZ = gyroZ(tC:tCF);
    yaw_from_IMU = yaw_from_IMU(tC:tCF);
    IMU_t = IMU_t(tC:tCF);
    magX = magX(tC:tCF);
    magY = magY(tC:tCF);
    MF_t = MF_t(tC:tCF);
    
    %HI and SI data correction
    bHIX = [1.21376292760541e-07]; %from part 1 calibration
    bHIY = [-8.43138027048529e-06]; %from part 1 calibration
    magHI = [transpose(magX)-bHIX; transpose(magY)-bHIY];
    bSI =   eye(2); %no soft iron effect observed, so this can stay as I
    magCor = bSI*magHI;
    magCor = transpose(magCor);

    %Calculate Yaw
    yaw_from_mag = atan2(-magCor(:,2),magCor(:,1));
    yaw_from_gyro = cumtrapz(IMU_t,gyroZ);
    
    %mag Yaw is noisy, so I will smooth it
    window = 200;
    yaw_from_mag_smooth = smoothdata(yaw_from_mag,'movmean',window);
     
    %Filtering Yaw
    FHpass = .15; %passband frequency between 0 and Fs/2
    FLpass = .1;
    yaw_from_gyro_HP = highpass(yaw_from_gyro,FHpass,Fs);
    yaw_from_mag_LP = lowpass(yaw_from_mag_smooth,FLpass,Fs);
    yaw_comp_filter = yaw_from_gyro_HP + yaw_from_mag_LP;
    yaw_comp_filter = wrapToPi(yaw_comp_filter);

%VELOCITY DERIVATION (FROM PART 2)
    window = 200;
    accX_smooth = smoothdata(accX,'movmean',window);
    accX3 = accX_smooth;
    frameLength = 20;
    
    movFrame = [];    
    for i=1:frameLength
        movFrame = [movFrame;accX3(i)];
    end
    
    displacement = mean(movFrame); %start at acc = 0
    for i=1:frameLength
        accX3(i) = accX3(i)-displacement;
    end
    
    for i=frameLength+1:length(accX3)
        accX3(i) = accX3(i) - displacement;
        
        instMean = mean(movFrame);
        instStd = std(movFrame);
        
        if instStd < 0.001
            %disp("from "+(i-frameLength)+" to "+i+": "+instMean);
            displacement = displacement + instMean;
            accX3(i-frameLength:i-1) = zeros([frameLength 1]);
        end        
        
        movFrame(1)=[];
        movFrame = [movFrame;accX3(i)];
    end
    
    %velocity from linear acceleration
    velX_from_accX3 = cumtrapz(IMU_t,accX3);
    
%INTEGRATE VELOCITY
%1. x_dd = x_dd; integrate for v = x_d. y_dd = omega*x_d
    %velocity from linear acceleration
    velX_from_accX = cumtrapz(IMU_t,accX);
    window = 200;
    accX_smooth = smoothdata(accX,'movmean',window);
    calculated_accY = gyroZ.*velX_from_accX;

    figure(3)
    hold on
    scatter(IMU_t,accY,'b.');
    scatter(IMU_t,calculated_accY,'r.');
    xlabel('Time (s)'); ylabel('Linear Acceleration (m/s^2)');
    legend({'accY','Calculated AccY'},'Location','east');
    title("Linear Acceleration vs Time");
    hold off

%ESTIMATE TRAJECTORY
    %Trajectory Attempt 1
    %velocity in reference frame    
    v_East = velX_from_accX3.*cos(yaw_comp_filter);
    v_North = velX_from_accX3.*sin(yaw_comp_filter);
    
    x_East = cumtrapz(IMU_t,v_East);
    x_North = cumtrapz(IMU_t,v_North);
    
    %Graph
    figure(4)
    hold on    
    scatter(UTMEast,UTMNorth,'r.');    
    scatter(x_East,x_North,'b.');
    xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');zlabel('Time (s)');
    title('East Heading vs North Heading');
    legend({'UTM/GPS','X from Acc'},'Location','southeast');
    grid on;
    hold off
    
    %Trajectory Attempt 2
    rotation = -108*pi/180;
    v_East = velX_from_accX3.*cos(yaw_comp_filter+rotation);
    v_North = -velX_from_accX3.*sin(yaw_comp_filter+rotation);
    x_East = cumtrapz(IMU_t,v_East);
    x_North = cumtrapz(IMU_t,v_North);
    
    figure(5)
    hold on    
    scatter(UTMEast,UTMNorth,'r.');    
    scatter(x_East,x_North,'b.');
    xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');zlabel('Time (s)');
    title('East Heading vs North Heading');
    legend({'UTM/GPS','X from Acc'},'Location','southeast');
    grid on;
    hold off
    
    %Trajectory Attempt 3
    rotation = -108*pi/180;
    v_East = velX_from_accX3.*cos(yaw_comp_filter+rotation);
    v_North = -velX_from_accX3.*sin(yaw_comp_filter+rotation);
    
    x_East = cumtrapz(IMU_t,v_East);
    x_North = cumtrapz(IMU_t,v_North);
    
    sF = 0.45;%scaling factors, try 0.7, 0.45, 
    x_East = x_East*sF;
    x_North = x_North*sF;
    
    figure(6)
    hold on    
    scatter(UTMEast,UTMNorth,'r.');    
    scatter(x_East,x_North,'b.');
    xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');
    title('East Heading vs North Heading with SF: ' + string(round(sF,2)));
    legend({'UTM/GPS','X from Acc'},'Location','southeast');
    grid on;
    hold off
    
    %Trajectory Attempt 4 (different scaling factor for each axis)
    rotation = -108*pi/180;
    v_East = velX_from_accX3.*cos(yaw_comp_filter+rotation);
    v_North = -velX_from_accX3.*sin(yaw_comp_filter+rotation);
    
    x_East = cumtrapz(IMU_t,v_East);
    x_North = cumtrapz(IMU_t,v_North);
    
    sFE = 0.6;
    sFN = .3; 
    x_East = x_East*sFE;
    x_North = x_North*sFN;
    
    figure(7)
    hold on    
    scatter(UTMEast,UTMNorth,'r.');    
    scatter(x_East,x_North,'b.');
    xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');
    title('East Heading vs North Heading with SFEast: ' ...
        + string(round(sFE,2)) + ', SFNorth: ' + string(round(sFN,2)));
    legend({'UTM/GPS','X from Acc'},'Location','southeast');
    grid on;
    hold off
    
%ESTIMATE Xc  
    leftB = 15000;
    rightB = 23000;
    
    window = 200;
    gyroZ_smooth = smoothdata(gyroZ,'movmean',window);
    figure(8)
    hold on
    scatter(IMU_t(leftB:rightB),gyroZ_smooth(leftB:rightB),'g.');
    xlabel('Time (s)'); ylabel('Angular Velocity (rad/s)');
    legend({'GyroZ'},'Location','east');
    title("Angular Velocity vs Time");
    hold off
    
    figure(9)
    hold on
    scatter(IMU_t(leftB:rightB),velX_from_accX3(leftB:rightB),'g.');
    plot(IMU_t(18956),velX_from_accX3(18956),'b*',IMU_t(20471),velX_from_accX3(20471),'b*',...
        IMU_t(21912),velX_from_accX3(21912),'r*');
    xlabel('Time (s)'); ylabel('Velocity (m/s)');
    legend({'velX'},'Location','east');
    title("Velocity vs Time");
    hold off
    
    x_East = cumtrapz(IMU_t,v_East);
    x_North = cumtrapz(IMU_t,v_North);    
    sF = 0.45;%scaling factors, try 0.7, 0.45, 
    x_East = x_East*sF;
    x_North = x_North*sF;    
    figure(10)
    hold on    
    scatter(UTMEast,UTMNorth,'k.');    
    scatter(x_East(leftB:rightB),x_North(leftB:rightB),'g.');
%     plot(x_East(16645),x_North(16645),'b*',x_East(16916),x_North(16916),'r*',...
%         x_East(17255),x_North(17255),'b*',x_East(18073),x_North(18073),'r*',...
%         x_East(19134),x_North(19134),'b*',x_East(19763),x_North(19763),'r*');
     plot(x_East(18956),x_North(18956),'b*',x_East(20471),x_North(20471),'b*',...
        x_East(21912),x_North(21912),'r*');
    xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');
    title('East Heading vs North Heading with SF: ' + string(round(sF,2)));
    legend({'UTM/GPS','X from Acc'},'Location','southeast');
    grid on;
    hold off
    
    leftB = 19795;
    rightB = 20981;
    %v = V + ω x r
    %because r = (xc,0,0) and ω = (0,0,ω), cross product can be replaced
    %with ω*Xc
    v = velX_from_accX3(leftB:rightB);
    V = (1.61332 +3.75969)/2;
    omega = gyroZ_smooth(leftB:rightB);
    Xc = (v-V)./omega;
    figure(11);
    plot(Xc);    
    xlabel('Xc Estimate (m)'); ylabel('Datapoint');

    leftB = 20400;
    rightB = 20500;
    %v = V + ω*Xc
    v = velX_from_accX3(leftB:rightB);
    V = (1.61332 +3.75969)/2;
    omega = gyroZ_smooth(leftB:rightB);
    Xc = (v-V)./omega;
    figure(12);
    plot(Xc);%on the order of 25m    
    xlabel('Xc Estimate (m)'); ylabel('Datapoint');
    
    leftB = 15000;
    rightB = 15800;
    %v = V + ω*Xc
    v = velX_from_accX3(leftB:rightB);
    V = min(v);
    omega = gyroZ_smooth(leftB:rightB);
    Xc = (v-V)./omega;
    figure(13);
    plot(Xc);    
    xlabel('Xc Estimate (m)'); ylabel('Datapoint');
    mean(Xc);%on the order of 4cm
    
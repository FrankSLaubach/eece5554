#!/usr/bin/env python

import rospy
import serial
import utm
from std_msgs.msg import String
from lab2.msg import Tracker

def tracking_driver(data):
    pub = rospy.Publisher('chatter', Tracker, queue_size=10)
    rospy.init_node('Tracking', anonymous=True)
    rate = rospy.Rate(1) # Rate of sampling [hz]
    gps_msg = Tracker()
    #while not rospy.is_shutdown():
    info = str(data)
    endnum = len(info)
    parse1 = info[1:endnum]
    parse2 = parse1.split(',')
    
    
    
    #extract lat long alt
    #sometimes the gps outputs no data, so this section uses 0 as placeholder values in case no data can be found.
    latVal = 0.0
    longVal = 0.0
    alt = 0.0
    quality = 0.0
    if len(parse2)>9:
	    if check_float(parse2[2]):
	    	latVal = float(parse2[2])
	    	latVal = latVal/100
	    	DDMM = str(latVal).split('.')
	    	DDMM[1] = DDMM[1][0:2]+'.'+DDMM[1][2:len(DDMM[1])]
	    	DDMM[1] = float(DDMM[1])/60
	    	latVal = round(float(DDMM[0])+DDMM[1],9)
	    	if parse2[3] == 'S':
	    		latVal = -1*latVal
	    if check_float(parse2[4]):
	    	longVal = float(parse2[4])
	    	longVal = longVal/100
	    	DDMM = str(longVal).split('.')
	    	DDMM[1] = DDMM[1][0:2]+'.'+DDMM[1][2:len(DDMM[1])]
	    	DDMM[1] = float(DDMM[1])/60
	    	longVal = round(float(DDMM[0])+DDMM[1],9)
	    	if parse2[5] == 'W':
	    		longVal = -1*longVal
	    	
	    if check_float(parse2[9]):
	    	alt = float(parse2[9])
	    if check_float(parse2[6]):
	    	quality = float(parse2[6])
    
    #calculate utm_data, with an error catcher.
    try:
    	utm_data = utm.from_latlon(latVal, longVal)
    except utm.error.OutOfRangeError: 
    	utm_data = [0,0,0,0]
    utm_e = utm_data[0]
    utm_n = utm_data[1]
    zoneVal = utm_data[2]
    letterVal = utm_data[3]
    
    ### Send all values to GPS.msg
    gps_msg.header.seq+=0
    gps_msg.header.stamp = rospy.Time.now()
    gps_msg.header.frame_id = 'GNGGA'
    gps_msg.latitude = latVal
    gps_msg.longitude = longVal
    gps_msg.altitude = alt
    gps_msg.utm_easting = utm_e
    gps_msg.utm_northing = utm_n
    gps_msg.zone = zoneVal
    gps_msg.letter = letterVal
    gps_msg.quality = quality

    rospy.loginfo(gps_msg)
    pub.publish(gps_msg)
    rate.sleep()
    
def check_float(potential_float):
    try:
        float(potential_float)
        return True
    except ValueError:
        return False

if __name__ == '__main__':
    SENSOR_NAME = "GPS"
    serial_port = rospy.get_param('~port','/dev/ttyACM0')
    serial_baud = rospy.get_param('~baudrate',57600)
    port = serial.Serial(serial_port, serial_baud, timeout=3.)
           
    line = port.readline() # gets the data from the sensor
       
    try:
        while not rospy.is_shutdown():
            line = port.readline()
            reading = str(line)
            #test = '$GPGGA,134658.00,5116.9792,N,11412.3003,W,2,09,1.0,1048.47,M,-16.27,M,08,AAAA*60'
            #tracking_driver(test)
            if reading.startswith("b'$GNGGA"):
            	 tracking_driver(line)
            
            
    except rospy.ROSInterruptException:
        port.close()
    
    #except serial.serialutil.SerialException:
        #rospy.loginfo("Shutting down GPS node...")

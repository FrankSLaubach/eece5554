%OPENING DATA
%Opening data from other team
bagPath = ["ROSbag/Soccer_field_stationary1.bag","ROSbag/ISEC_moving.bag",...
    "ROSbag/ISEC_stationary.bag","ROSbag/Soccer_field_moving.bag",...
    "ROSbag/Soccer_field_stationary2.bag"];
bagSelect = {};
bagInfo = {};
bagTopic = {};

for i=1:length(bagPath)
    bagSelect = [bagSelect,rosbag(bagPath(i))];
    bagInfo = [bagInfo,rosbag('info',bagPath(i))];
    bagTopic = [bagTopic,select(bagSelect(i),'Topic','gpstopic')];
    bagRead = readMessages(bagTopic(i),'DataFormat','struct');
    if i == 1 openStill1 = bagRead;
    elseif i == 2 obscuredMoving = bagRead;
    elseif i == 3 obscuredStill = bagRead;
    elseif i == 4 openMoving = bagRead;
    elseif i == 5 openStill2 = bagRead;
    end
end

%Opening data from our old attempts with bad hardware
i = 6;
bagPath = [bagPath,"Oldbags/ObscuredStill.bag"];
bagSelect = [bagSelect,rosbag(bagPath(i))];
bagInfo = [bagInfo,rosbag('info',bagPath(i))];
bagTopic = [bagTopic,select(bagSelect(i),'Topic','chatter')];
bagRead = readMessages(bagTopic(i),'DataFormat','struct');
trial0 = bagRead;

%EXCTRACTING BAG DATA
%Trial 0. Old data from our attempts, has float and single
T0East = cellfun(@(m) double(m.UtmEasting),trial0);
T0EastMean = mean(T0East);
T0East = T0East-T0EastMean; %Center data around mean
T0EastSing = [];
T0EastFloat = [];
T0North = cellfun(@(m) double(m.UtmNorthing),trial0);
T0NorthMean = mean(T0North);
T0North = T0North-T0NorthMean; %Center data around mean
T0NorthSing = [];
T0NorthFloat = [];
T0Alt = cellfun(@(m) double(m.Altitude),trial0);
T0AltSing = [];
T0AltFloat = [];
T0Time = bagSelect(6).MessageList.Time-bagSelect(6).StartTime;
T0TimeSing = [];
T0TimeFloat = [];
T0q = cellfun(@(m) double(m.Quality),trial0);
for i=1:length(T0q)
    if T0q(i) == 1
        T0EastSing = [T0EastSing;T0East(i)];
        T0NorthSing = [T0NorthSing;T0North(i)];
        T0AltSing = [T0AltSing;T0Alt(i)];
        T0TimeSing = [T0TimeSing;T0Time(i)];
    elseif T0q(i) == 5
        T0EastFloat = [T0EastFloat;T0East(i)];
        T0NorthFloat = [T0NorthFloat;T0North(i)];
        T0AltFloat = [T0AltFloat;T0Alt(i)];
        T0TimeFloat = [T0TimeFloat;T0Time(i)];
    end
end
%Displaying Mean, STD
disp("Trial E: "+T0EastMean+", "+std(T0East))
disp("Trial N: "+T0NorthMean+", "+std(T0North))
disp("Trial Alt: "+mean(T0Alt)+", "+std(T0Alt))
disp("Trial E Sing: "+mean(T0EastSing)+", "+std(T0EastSing))
disp("Trial N Sing: "+mean(T0NorthSing)+", "+std(T0NorthSing))
disp("Trial Alt Sing: "+mean(T0AltSing)+", "+std(T0AltSing))
disp("Trial E Float: "+mean(T0EastFloat)+", "+std(T0EastFloat))
disp("Trial N Float: "+mean(T0NorthFloat)+", "+std(T0NorthFloat))
disp("Trial Alt Float: "+mean(T0AltFloat)+", "+std(T0AltFloat))
disp(' ')

%Open Still 1. Data from other team.
openS1East = cellfun(@(m) double(struct(m.UtmEasting).Data),openStill1);
openS1EastMean = mean(openS1East);
openS1East = openS1East-openS1EastMean; %Center data around mean
openS1North = cellfun(@(m) double(struct(m.UtmNorthing).Data),openStill1);
openS1NorthMean = mean(openS1North);
openS1North = openS1North-openS1NorthMean; %Center data around mean
openS1Alt = cellfun(@(m) double(struct(m.Altitude).Data),openStill1);
openS1Time = bagSelect(1).MessageList.Time-bagSelect(1).StartTime;
openS1q = cellfun(@(m) double(struct(m.FixQuality).Data),openStill1);
%Displaying Mean, STD
disp("Open Still E: "+openS1EastMean+", "+std(openS1East))
disp("Open Still N: "+openS1NorthMean+", "+std(openS1North))
disp("Open Still Alt: "+mean(openS1Alt)+", "+std(openS1Alt))
disp(' ')

%Open Still 2. Data from other team. This data isn't as good as the...
%previous one, so I will not use it in my analysis.
% openS2East = cellfun(@(m) double(struct(m.UtmEasting).Data),openStill2);
% openS2EastMean = mean(openS2East);
% openS2East = openS2East-openS2EastMean; %Center data around mean
% openS2EastFloat = [];
% openS2EastFixed = [];
% openS2North = cellfun(@(m) double(struct(m.UtmNorthing).Data),openStill2);
% openS2NorthMean = mean(openS2North);
% openS2North = openS2North-openS2NorthMean; %Center data around mean
% openS2NorthFloat = [];
% openS2NorthFixed = [];
% openS2Alt = cellfun(@(m) double(struct(m.Altitude).Data),openStill2);
% openS2AltFloat = [];
% openS2AltFixed = [];
% openS2q = cellfun(@(m) double(struct(m.FixQuality).Data),openStill2);
% for i=1:length(openS2q)
%     if openS2q(i) == 4
%         openS2EastFixed = [openS2EastFixed;openS2East(i)];
%         openS2NorthFixed = [openS2NorthFixed;openS2North(i)];
%         openS2AltFixed = [openS2AltFixed;openS2Alt(i)];
%     elseif openS2q(i) == 5
%         openS2EastFloat = [openS2EastFloat;openS2East(i)];
%         openS2NorthFloat = [openS2NorthFloat;openS2North(i)];
%         openS2AltFloat = [openS2AltFloat;openS2Alt(i)];
%     end
% end

%Open Moving. Data from other team
openMEast = cellfun(@(m) double(struct(m.UtmEasting).Data),openMoving);
openMEastMean = mean(openMEast);
openMEast = openMEast-openMEastMean; %Center data around mean
openMNorth = cellfun(@(m) double(struct(m.UtmNorthing).Data),openMoving);
openMNorthMean = mean(openMNorth);
openMNorth = openMNorth-openMNorthMean; %Center data around mean
openMAlt = cellfun(@(m) double(struct(m.Altitude).Data),openMoving);
openMTime = bagSelect(4).MessageList.Time-bagSelect(4).StartTime;
openMq = cellfun(@(m) double(struct(m.FixQuality).Data),openMoving);
%Best Fit Line %https://www.mathworks.com/matlabcentral/answers/377139-how-to-plot-best-fit-line
x = openMEast(1:280);
y = openMNorth(1:280);
coefficients = polyfit(x, y, 1);
openMxFit = linspace(max(x), min(x), 280);
openMyFit = polyval(coefficients , openMxFit);
openMyDiff = y-openMyFit.';
%Displaying Mean, STD
disp("Open Moving E: "+openMEastMean+", "+std(openMEast))
disp("Open Moving N: "+openMNorthMean+", "+std(openMNorth))
disp("Open Moving Alt: "+mean(openMAlt)+", "+std(openMAlt))
disp("Open Moving N RMS: " + rms(openMyDiff));
disp("Open Moving N R^2: " + corrcoef(openMyDiff)^2);
disp(' ')

%Obscured Still. Data from other team
obsSEast = cellfun(@(m) double(struct(m.UtmEasting).Data),obscuredStill);
obsSEastMean = mean(obsSEast);
obsSEast = obsSEast-obsSEastMean; %Center data around mean
obsSEastFloat = [];
obsSEastFixed = [];
obsSNorth = cellfun(@(m) double(struct(m.UtmNorthing).Data),obscuredStill);
obsSNorthMean = mean(obsSNorth);
obsSNorth = obsSNorth-obsSNorthMean; %Center data around mean
obsSNorthFloat = [];
obsSNorthFixed = [];
obsSAlt = cellfun(@(m) double(struct(m.Altitude).Data),obscuredStill);
obsSAltFloat = [];
obsSAltFixed = [];
obsSTime = bagSelect(3).MessageList.Time-bagSelect(3).StartTime;
obsSTimeFloat = [];
obsSTimeFixed = [];
obsSq = cellfun(@(m) double(struct(m.FixQuality).Data),obscuredStill);
for i=1:length(obsSq)
    if obsSq(i) == 4
        obsSEastFixed = [obsSEastFixed;obsSEast(i)];
        obsSNorthFixed = [obsSNorthFixed;obsSNorth(i)];
        obsSAltFixed = [obsSAltFixed;obsSAlt(i)];
        obsSTimeFixed = [obsSTimeFixed;obsSTime(i)];
    elseif obsSq(i) == 5
        obsSEastFloat = [obsSEastFloat;obsSEast(i)];
        obsSNorthFloat = [obsSNorthFloat;obsSNorth(i)];
        obsSAltFloat = [obsSAltFloat;obsSAlt(i)];
        obsSTimeFloat = [obsSTimeFloat;obsSTime(i)];
    end
end
%Displaying Mean, STD
disp("Obs Still E: "+obsSEastMean+", "+std(obsSEast))
disp("Obs Still N: "+obsSNorthMean+", "+std(obsSNorth))
disp("Obs Still Alt: "+mean(obsSAlt)+", "+std(obsSAlt))
disp("Obs Still E Float: "+mean(obsSEastFloat)+", "+std(obsSEastFloat))
disp("Obs Still N Float: "+mean(obsSNorthFloat)+", "+std(obsSNorthFloat))
disp("Obs Still Alt Float: "+mean(obsSAltFloat)+", "+std(obsSAltFloat))
disp("Obs Still E Fixed: "+mean(obsSEastFixed)+", "+std(obsSEastFixed))
disp("Obs Still N Fixed: "+mean(obsSNorthFixed)+", "+std(obsSNorthFixed))
disp("Obs Still Alt Fixed: "+mean(obsSAltFixed)+", "+std(obsSAltFixed))
disp(' ')

%Obscured Moving. Data from other team
obsMEast = cellfun(@(m) double(struct(m.UtmEasting).Data),obscuredMoving);
obsMEastMean = mean(obsMEast);
obsMEast = obsMEast-obsMEastMean; %Center data around mean
obsMEastFloat = [];
obsMEastFixed = [];
obsMNorth = cellfun(@(m) double(struct(m.UtmNorthing).Data),obscuredMoving);
obsMNorthMean = mean(obsMNorth);
obsMNorth = obsMNorth-obsMNorthMean; %Center data around mean
obsMNorthFloat = [];
obsMNorthFixed = [];
obsMAlt = cellfun(@(m) double(struct(m.Altitude).Data),obscuredMoving);
obsMAltFloat = [];
obsMAltFixed = [];
obsMTime = bagSelect(2).MessageList.Time-bagSelect(2).StartTime;
obsMTimeFloat = [];
obsMTimeFixed = [];
obsMq = cellfun(@(m) double(struct(m.FixQuality).Data),obscuredMoving);
for i=1:length(obsMq)
    if obsMq(i) == 4
        obsMEastFixed = [obsMEastFixed;obsMEast(i)];
        obsMNorthFixed = [obsMNorthFixed;obsMNorth(i)];
        obsMAltFixed = [obsMAltFixed;obsMAlt(i)];
        obsMTimeFixed = [obsMTimeFixed;obsMTime(i)];
    elseif obsMq(i) == 5
        obsMEastFloat = [obsMEastFloat;obsMEast(i)];
        obsMNorthFloat = [obsMNorthFloat;obsMNorth(i)];
        obsMAltFloat = [obsMAltFloat;obsMAlt(i)];
        obsMTimeFloat = [obsMTimeFloat;obsMTime(i)];
    end
end
%Best Fit Line %https://www.mathworks.com/matlabcentral/answers/377139-how-to-plot-best-fit-line
x = obsMEastFloat(153:632);
y = obsMNorthFloat(153:632);
coefficients = polyfit(x, y, 1);
obsMxFit = linspace(min(x), max(x), 480);
obsMyFit = polyval(coefficients , obsMxFit);
obsMyDiff = y-obsMyFit.';
%Displaying Mean, STD
disp("Obs Moving E: "+obsMEastMean+", "+std(obsMEast))
disp("Obs Moving N: "+obsMNorthMean+", "+std(obsMNorth))
disp("Obs Moving Alt: "+mean(obsMAlt)+", "+std(obsMAlt))
disp("Obs Moving E Float: "+mean(obsMEastFloat)+", "+std(obsMEastFloat))
disp("Obs Moving N Float: "+mean(obsMNorthFloat)+", "+std(obsMNorthFloat))
disp("Obs Moving Alt Float: "+mean(obsMAltFloat)+", "+std(obsMAltFloat))
disp("Obs Moving E Fixed: "+mean(obsMEastFixed)+", "+std(obsMEastFixed))
disp("Obs Moving N Fixed: "+mean(obsMNorthFixed)+", "+std(obsMNorthFixed))
disp("Obs Moving Alt Fixed: "+mean(obsMAltFixed)+", "+std(obsMAltFixed))
disp("Obs Moving N Fixed RMS: " + rms(obsMyDiff));
disp("Obs Moving N R^2: " + corrcoef(obsMyDiff)^2);
disp(' ')




% %PLOT UTM Graphs
figure(1)
hold on
scatter(T0EastSing,T0NorthSing,'r.');
scatter(T0EastFloat,T0NorthFloat,'b.');
xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');
legend({'Single','Float'},'Location','southeast');
title('Easting vs Northing for Still Rover in Obscured Environment (Trial)');
hold off

figure(2)
hold on
scatter(openMEast,openMNorth,'g.');
plot(openMxFit, openMyFit, '.-', 'LineWidth', 2); % Plot fitted line.
xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');
legend({'Fixed'},'Location','southeast');
title('Easting vs Northing for Moving Rover in Open Environment');
hold off

figure(3)
hold on
scatter(openS1East,openS1North,'g.');
xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');
legend({'Fixed'},'Location','southeast');
title('Easting vs Northing for Still Rover in Open Environment');
hold off

figure(4)
hold on
scatter(obsMEastFloat,obsMNorthFloat,'b.');
scatter(obsMEastFixed,obsMNorthFixed,'g.');
plot(obsMxFit, obsMyFit, '.-', 'LineWidth', 2); % Plot fitted line.
xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');
legend({'Float','Fixed'},'Location','southeast');
title('Easting vs Northing for Moving Rover in Obscured Environment');
hold off

figure(5)
hold on
scatter(obsSEastFloat,obsSNorthFloat,'b.');
scatter(obsSEastFixed,obsSNorthFixed,'g.');
xlabel('UTM Easting (m)'); ylabel('UTM Northing (m)');
legend({'Float','Fixed'},'Location','southeast');
title('Easting vs Northing for Still Rover in Obscured Environment');
hold off

%PLOT ALTITUDE Graphs
figure(6)
hold on
scatter(T0TimeSing,T0AltSing,'r.');
scatter(T0TimeFloat,T0AltFloat,'b.');
xlabel('Time (s)'); ylabel('Altitude (m)');
legend({'Single','Float'},'Location','southeast');
title('Altitude vs Time for Still Rover in Obscured Environment (Trial)');
hold off

figure(7)
hold on
scatter(openMTime,openMAlt,'g.');
xlabel('Time (s)'); ylabel('Altitude (m)');
legend({'Fixed'},'Location','southeast');
title('Altitude vs Time for Moving Rover in Open Environment');
hold off

figure(8)
hold on
scatter(openS1Time,openS1Alt,'g.');
xlabel('Time (s)'); ylabel('Altitude (m)');
legend({'Fixed'},'Location','southeast');
title('Altitude vs Time for Still Rover in Open Environment');
hold off

figure(9)
hold on
scatter(obsMTimeFloat,obsMAltFloat,'b.');
scatter(obsMTimeFixed,obsMAltFixed,'g.');
xlabel('Time (s)'); ylabel('Altitude (m)');
legend({'Float','Fixed'},'Location','southeast');
title('Altitude vs Time for Moving Rover in Obscured Environment');
hold off

figure(10)
hold on
scatter(obsSTimeFloat,obsSAltFloat,'b.');
scatter(obsSTimeFixed,obsSAltFixed,'g.');
xlabel('Time (s)'); ylabel('Altitude (m)');
legend({'Float','Fixed'},'Location','southeast');
title('Altitude vs Time for Still Rover in Obscured Environment');
hold off
%EECE 5554 Lab 3 IMU, 5 Hour Test
%https://www.mathworks.com/help/nav/ug/inertial-sensor-noise-analysis-using-allan-variance.html

%open bag
%bagPath = 'ROSbags/IMUten1.bag';
%bagPath = 'ROSbags/FiveHours.bag'; %Alternate, collected evening
bagPath = 'ROSbags/FiveHours2.bag'; %Used, collected at night
bagSelect = rosbag(bagPath);
bagInfo = rosbag('info',bagPath);
IMUtopic = select(bagSelect,'Topic','IMU');
IMUread = readMessages(IMUtopic,'DataFormat','struct');

%extract data
gyroX = cellfun(@(m) struct(m.AngularVelocity).X,IMUread);
gyroY = cellfun(@(m) struct(m.AngularVelocity).Y,IMUread);
gyroZ = cellfun(@(m) struct(m.AngularVelocity).Z,IMUread);
accX = cellfun(@(m) struct(m.LinearAcceleration).X,IMUread);
accY = cellfun(@(m) struct(m.LinearAcceleration).Y,IMUread);
accZ = cellfun(@(m) struct(m.LinearAcceleration).Z,IMUread);

%for 5Hours2
%a garbage truck showed up so I am removing the last 1000 seconds
gyroX = gyroX(1:length(gyroX)-1000*40);
gyroY = gyroY(1:length(gyroY)-1000*40);
gyroZ = gyroZ(1:length(gyroZ)-1000*40);
accX = accX(1:length(accX)-1000*40);
accY = accY(1:length(accY)-1000*40);
accZ = accZ(1:length(accZ)-1000*40);

%Fix Time
IMUtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,IMUread);
IMUtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,IMUread));
IMUtimeNS = IMUtimeNS/1000000000;
IMU_t = double(IMUtimeS) + IMUtimeNS;
IMU_t = IMU_t-IMU_t(1);


%Alan Variance
%https://www.mathworks.com/help/nav/ug/inertial-sensor-noise-analysis-using-allan-variance.html
t0 = 1/40; %for 40 hz
type = ["GyroX"; "GyroY"; "GyroZ";"AccX"; "AccY"; "AccZ"];
list = [gyroX,gyroY,gyroZ,accX,accY,accZ];

for x=1:6
    omega = list(:,x);
    theta = cumsum(omega, 1)*t0;

    %Alan Variance Calculation
    maxNumM = 100;
    L = size(theta, 1);
    maxM = 2.^floor(log2(L/2));
    m = logspace(log10(1), log10(maxM), maxNumM).';
    m = ceil(m); % m must be an integer.
    m = unique(m); % Remove duplicates.

    tau = m*t0;

    avar = zeros(numel(m), 1);
    for i = 1:numel(m)
        mi = m(i);
        avar(i,:) = sum( ...
            (theta(1+2*mi:L) - 2*theta(1+mi:L-mi) + theta(1:L-2*mi)).^2, 1);
    end
    avar = avar ./ (2*tau.^2 .* (L - 2*m));
    adev = sqrt(avar);
    
    %Angle Random Walk
    slope = -0.5;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));
    
    b = logadev(i) - slope*logtau(i);
    logN = slope*log(1) + b;
    N = 10^logN;
    disp(type(x)+" N: "+N)

    tauN = 1;
    lineN = N ./ sqrt(tau);
    
    %Rate Random Walk
    slope = 0.5;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));

    b = logadev(i) - slope*logtau(i);
    logK = slope*log10(3) + b;
    K = 10^logK;
    disp(type(x)+" K: "+K);
    
    tauK = 3;
    lineK = K .* sqrt(tau/3);
    
    %Bias Instability
    slope = 0;
    logtau = log10(tau);
    logadev = log10(adev);
    dlogadev = diff(logadev) ./ diff(logtau);
    [~, i] = min(abs(dlogadev - slope));

    b = logadev(i) - slope*logtau(i);
    scfB = sqrt(2*log(2)/pi);
    logB = b - log10(scfB);
    B = 10^logB;
    disp(type(x)+" B: "+B)

    tauB = tau(i);
    lineB = B * scfB * ones(size(tau));
    
    %Plot Allan Deviation with all Lines
    tauParams = [tauN, tauK, tauB];
    params = [N, K, scfB*B];
    figure
    loglog(tau, adev, tau, [lineN, lineK, lineB], '--', ...
        tauParams, params, 'o')
    title("Allan Deviation with Noise Parameters for " + type(x))
    xlabel('\tau')
    ylabel('\sigma(\tau)')
    
    if x >= 1 & x <= 3
        legend('$\sigma (rad/s)$', '$\sigma_N ((rad/s)/\sqrt{Hz})$', ...
            '$\sigma_K ((rad/s)\sqrt{Hz})$', '$\sigma_B (rad/s)$', 'Interpreter', 'latex')
    elseif x >= 4 & x <= 6
        legend('$\sigma (m/s^2)$', '$\sigma_N ((m/s^2)/\sqrt{Hz})$', ...
            '$\sigma_K ((m/s^2)\sqrt{Hz})$', '$\sigma_B (m/s^2)$', 'Interpreter', 'latex')
    end
    text(tauParams, params, {'N', 'K', '0.664B'})
    grid on
    axis equal
end
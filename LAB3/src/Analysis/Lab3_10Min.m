%EECE 5554 Lab 3 IMU, 10 Minute Test

%open bag
bagPath = 'ROSbags/IMUten1.bag';
%bagPath = 'ROSbags/FiveHours.bag';
%bagPath = 'ROSbags/FiveHours2.bag';
bagSelect = rosbag(bagPath);
bagInfo = rosbag('info',bagPath);
IMUtopic = select(bagSelect,'Topic','IMU');
MFtopic = select(bagSelect,'Topic','MF');
IMUread = readMessages(IMUtopic,'DataFormat','struct');
MFread = readMessages(MFtopic,'DataFormat','struct');
%rosbag info 'ROSbags/IMUten1.bag';
%IMUread{1}
%MFread{1}

%extract data
qX = cellfun(@(m) struct(m.Orientation).X,IMUread);
qY = cellfun(@(m) struct(m.Orientation).Y,IMUread);
qZ = cellfun(@(m) struct(m.Orientation).Z,IMUread);
qW = cellfun(@(m) struct(m.Orientation).W,IMUread);
gyroX = cellfun(@(m) struct(m.AngularVelocity).X,IMUread);
gyroY = cellfun(@(m) struct(m.AngularVelocity).Y,IMUread);
gyroZ = cellfun(@(m) struct(m.AngularVelocity).Z,IMUread);
accX = cellfun(@(m) struct(m.LinearAcceleration).X,IMUread);
accY = cellfun(@(m) struct(m.LinearAcceleration).Y,IMUread);
accZ = cellfun(@(m) struct(m.LinearAcceleration).Z,IMUread);
magX = cellfun(@(m) struct(m.MagneticField_).X,MFread);
magY = cellfun(@(m) struct(m.MagneticField_).Y,MFread);
magZ = cellfun(@(m) struct(m.MagneticField_).Z,MFread);

%quaternion to euler
%https://www.mathworks.com/help/robotics/ref/quat2eul.html
eul = quat2eul([qW qX qY qZ]); %input is wxyz, output is zyx

%Fix Time
IMUtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,IMUread);
IMUtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,IMUread));
IMUtimeNS = IMUtimeNS/1000000000;
IMU_t = double(IMUtimeS) + IMUtimeNS;
IMU_t = IMU_t-IMU_t(1);

MFtimeS = cellfun(@(m) struct(m.Header).Stamp.Sec,MFread);
MFtimeNS = double(cellfun(@(m) struct(m.Header).Stamp.Nsec,MFread));
MFtimeNS = MFtimeNS/1000000000;
MF_t = double(MFtimeS) + MFtimeNS;
MF_t = MF_t - MF_t(1);

%Graph
figure(1) %only for 5 hr
hold on
scatter(IMU_t,eul(:,3),'r.');
scatter(IMU_t,eul(:,2),'b.');
scatter(IMU_t,eul(:,1),'g.');
xlabel('Time (s)'); ylabel('Orientation (rad)');
legend({'X','Y','Z'},'Location','east');
title("Orientation vs Time");
hold off
%Displaying Mean, STD
disp("OrientX: "+mean(eul(:,3))+", "+std(eul(:,3)))
disp("OrientY: "+mean(eul(:,2))+", "+std(eul(:,2)))
disp("OrientZ: "+mean(eul(:,1))+", "+std(eul(:,1)))

figure(2)
hold on
scatter(IMU_t,gyroX,'r.');
scatter(IMU_t,gyroY,'b.');
scatter(IMU_t,gyroZ,'g.');
xlabel('Time (s)'); ylabel('Angular Velocity (rad/s)');
legend({'GyroX','GyroY','GyroZ'},'Location','southeast');
title("Angular Velocity vs Time");
hold off
%Displaying Mean, STD
disp("GyroX: "+mean(gyroX)+", "+std(gyroX))
disp("GyroY: "+mean(gyroY)+", "+std(gyroY))
disp("GyroZ: "+mean(gyroZ)+", "+std(gyroZ))


figure(3)
hold on
scatter(IMU_t,accX,'r.');
scatter(IMU_t,accY,'b.');
scatter(IMU_t,accZ,'g.');
xlabel('Time (s)'); ylabel('Linear Acceleration (m/s^2)');
legend({'accX','accY','accZ'},'Location','east');
title("Linear Acceleration vs Time");
hold off
%Displaying Mean, STD
disp("AccX: "+mean(accX)+", "+std(accX))
disp("AccY: "+mean(accY)+", "+std(accY))
disp("AccZ: "+mean(accZ)+", "+std(accZ))

figure(4)
hold on
scatter(MF_t,magX,'r.');
scatter(MF_t,magY,'b.');
scatter(MF_t,magZ,'g.');
xlabel('Time (s)'); ylabel('Magnetic Fluctuation (Teslas)');
legend({'magX','magY','magZ'},'Location','east');
title("Magnetic Fluctuation vs Time");
hold off
%Displaying Mean, STD
disp("MagX: "+mean(magX)+", "+std(magX))
disp("MagY: "+mean(magY)+", "+std(magY))
disp("MagZ: "+mean(magZ)+", "+std(magZ))